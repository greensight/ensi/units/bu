<?php

namespace Database\Seeders;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Database\Seeder;

class StorePickupTimeSeeder extends Seeder
{
    public function run()
    {
        StorePickupTime::factory()->count(20)->create();
    }
}

<?php

namespace Database\Seeders;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Database\Seeder;

class StoreWorkingSeeder extends Seeder
{
    public function run()
    {
        StoreWorking::factory()->count(20)->create();
    }
}

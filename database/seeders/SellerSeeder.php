<?php

namespace Database\Seeders;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    public function run()
    {
        Seller::factory()->count(5)->create();
    }
}

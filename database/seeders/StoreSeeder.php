<?php

namespace Database\Seeders;

use App\Domain\Stores\Models\Store;
use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    public function run()
    {
        Store::factory()->count(10)->create();
    }
}

<?php

namespace Database\Seeders;

use App\Domain\SellerUsers\Models\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    public function run()
    {
        Operator::factory()->count(20)->create();
    }
}

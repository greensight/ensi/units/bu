<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('store_pickup_times')
            ->whereNull('pickup_time_start')
            ->update(['pickup_time_start' => '00:00:00']);
        DB::table('store_pickup_times')
            ->whereNull('pickup_time_end')
            ->update(['pickup_time_end' => '23:59:59']);

        Schema::table('store_pickup_times', function (Blueprint $table) {
            $table->time('pickup_time_start')->nullable(false)->change();
            $table->time('pickup_time_end')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_pickup_times', function (Blueprint $table) {
            $table->time('pickup_time_start')->nullable(true)->change();
            $table->time('pickup_time_end')->nullable(true)->change();
        });
    }
};

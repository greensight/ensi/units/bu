<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });
        Schema::table('merchants', function (Blueprint $table) {
            $table->text('fact_address')->nullable();
            $table->text('bank_address')->nullable();
            $table->string('bank_bik')->nullable();

            $table->renameColumn('payment_account_bank', 'bank');

            $table->dropColumn('correspondent_account_bank');
            $table->dropColumn('display_name');

            $table->integer('rating_id')->unsigned()->nullable();

            $table->foreign('rating_id')->references('id')->on('ratings');
        });

        Schema::table('merchants', function (Blueprint $table) {
            $table->string('legal_name')->nullable(false)->change();
        });

        Schema::table('operators', function (Blueprint $table) {
            $table->boolean('is_main')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropColumn('fact_address');
            $table->dropColumn('bank_address');
            $table->dropColumn('bank_bik');
            $table->dropColumn('rating_id');

            $table->renameColumn('bank', 'payment_account_bank');

            $table->text('legal_name')->nullable()->change();

            $table->string('correspondent_account_bank', 100)->nullable();
            $table->string('display_name');
        });

        Schema::table('operators', function (Blueprint $table) {
            $table->dropColumn('is_main');
        });

        Schema::dropIfExists('ratings');
    }
};

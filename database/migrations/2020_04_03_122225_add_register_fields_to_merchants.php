<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->text('storage_address')->nullable();
            $table->string('site')->nullable();
            $table->boolean('can_integration')->nullable();
            $table->text('sale_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropColumn('storage_address');
            $table->dropColumn('site');
            $table->dropColumn('can_integration');
            $table->dropColumn('sale_info');
        });
    }
};

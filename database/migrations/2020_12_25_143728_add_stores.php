<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('merchant_id')->unsigned();
            $table->string('xml_id')->nullable()->unique();
            $table->boolean('active')->default(true);
            $table->string('name');
            $table->json('address')->nullable();
            $table->string('timezone')->default("Europe/Moscow");

            $table->timestamps();

            $table->foreign('merchant_id')
                ->references('id')
                ->on('merchants')
                ->onDelete('cascade');
        });

        Schema::create('store_workings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('active')->default(true);
            $table->bigInteger('store_id')->unsigned();
            $table->tinyInteger('day')->unsigned();
            $table->string('working_start_time')->nullable();
            $table->string('working_end_time')->nullable();

            $table->timestamps();

            $table->foreign('store_id')
                ->references('id')
                ->on('stores')
                ->onDelete('cascade');
        });

        Schema::create('store_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('store_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();

            $table->timestamps();

            $table->foreign('store_id')
                ->references('id')
                ->on('stores')
                ->onDelete('cascade');
        });

        Schema::create('store_pickup_times', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('store_id')->unsigned();
            $table->tinyInteger('day')->unsigned();
            $table->string('pickup_time_code')->nullable();
            $table->time('pickup_time_start')->nullable();
            $table->time('pickup_time_end')->nullable();
            $table->time('cargo_export_time')->nullable();
            $table->tinyInteger('delivery_service')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('store_id')
                ->references('id')
                ->on('stores')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_pickup_times');
        Schema::dropIfExists('store_workings');
        Schema::dropIfExists('store_contacts');
        Schema::dropIfExists('stores');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sellers', function (Blueprint $table) {
            $table->dropColumn('rating_id');
        });

        Schema::dropIfExists('seller_integration_sys_params');
        Schema::dropIfExists('seller_integrations');
        Schema::dropIfExists('seller_ext_systems');
        Schema::dropIfExists('seller_settings');
        Schema::dropIfExists('api_users');
        Schema::dropIfExists('ratings');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('api_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('sellers');
        });

        Schema::create('seller_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            $table->string('name');
            $table->mediumText('value');
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('sellers');
        });

        Schema::create('seller_ext_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_id')->unsigned();
            $table->string('name');
            $table->string('driver', 50);
            $table->json('connection_params')->nullable();
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('sellers');
        });

        Schema::create('seller_integrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_ext_system_id')->unsigned();
            $table->string('name');
            $table->boolean('active')->default(true);
            $table->string('type', 100);
            $table->json('params')->nullable();
            $table->timestamp('last_start_at')->nullable();
            $table->timestamp('last_end_at')->nullable();
            $table->timestamps();

            $table->foreign('seller_ext_system_id')->references('id')->on('seller_ext_systems');
        });

        Schema::create('seller_integration_sys_params', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('seller_integration_id')->unsigned();
            $table->string('name');
            $table->mediumText('value');
            $table->timestamps();

            $table->foreign('seller_integration_id')->references('id')->on('seller_integrations');
        });

        Schema::table('sellers', function (Blueprint $table) {
            $table->integer('rating_id')->unsigned()->nullable();

            $table->foreign('rating_id')->references('id')->on('ratings');
        });
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->bigInteger('seller_id')->nullable()->change();
        });
    }

    public function down(): void
    {
        $selleId = DB::table('sellers')->first()->id;
        DB::table('stores')->whereNull('seller_id')->update(['seller_id' => $selleId]);

        Schema::table('stores', function (Blueprint $table) {
            $table->bigInteger('seller_id')->nullable(false)->change();
        });
    }
};

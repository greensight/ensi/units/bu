<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('sellers', function (Blueprint $table) {
            $table->dropColumn('storage_address');
            $table->dropColumn('can_integration');
            $table->dropColumn('city');
            $table->dropColumn('external_id');

            $table->renameColumn('sale_info', 'info');

            $table->dropColumn('legal_address');
            $table->dropColumn('fact_address');
            $table->dropColumn('bank_address');
        });

        Schema::table('sellers', function (Blueprint $table) {
            $table->jsonb('legal_address')->nullable();
            $table->jsonb('fact_address')->nullable();
            $table->jsonb('bank_address')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('sellers', function (Blueprint $table) {
            $table->text('storage_address')->nullable();
            $table->boolean('can_integration')->nullable();
            $table->string('city')->nullable();
            $table->string('external_id')->nullable(false);

            $table->renameColumn('info', 'sale_info');

            $table->text('legal_address')->change();
            $table->text('fact_address')->change();
            $table->text('bank_address')->change();
        });
    }
};

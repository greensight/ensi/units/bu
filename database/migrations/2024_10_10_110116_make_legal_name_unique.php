<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        $results = DB::table('sellers')
            ->groupBy('legal_name')
            ->havingRaw('count(*) > 1')
            ->pluck('legal_name')
            ->toArray();

        DB::table('sellers')
            ->whereIn('legal_name', $results)
            ->update(['legal_name' => DB::raw("concat(legal_name, '_', id)")]);

        Schema::table('sellers', function (Blueprint $table): void {
            $table->unique('legal_name');
        });
    }

    public function down(): void
    {
        Schema::table('sellers', function (Blueprint $table): void {
            $table->dropUnique('legal_name');
        });
    }
};

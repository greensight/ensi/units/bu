<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class Base
 */
return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('legal_name')->nullable();
            $table->string('display_name');
            $table->text('legal_address')->nullable();
            $table->string('inn', 30)->nullable();
            $table->string('kpp', 30)->nullable();
            $table->string('payment_account', 50)->nullable();
            $table->string('payment_account_bank', 100)->nullable();
            $table->string('correspondent_account', 50)->nullable();
            $table->string('correspondent_account_bank', 100)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::create('operators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->boolean('is_receive_sms')->default(false);
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
        });

        Schema::create('registration_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('operator_id')->unsigned()->nullable();
            $table->bigInteger('merchant_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1);
            ;
            $table->string('operator_name');
            $table->string('operator_phone')->nullable();
            $table->string('operator_email');
            $table->text('merchant_legal_name');
            $table->text('merchant_legal_address');
            $table->string('merchant_inn', 30);
            $table->string('merchant_kpp', 30);
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
            $table->foreign('operator_id')->references('id')->on('operators');
        });

        Schema::create('api_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
        });

        Schema::create('merchant_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned();
            $table->string('name');
            $table->mediumText('value');
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
        });

        Schema::create('merchant_ext_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned();
            $table->string('name');
            $table->string('driver', 50);
            $table->json('connection_params')->nullable();
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
        });

        Schema::create('merchant_integrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_ext_system_id')->unsigned();
            $table->string('name');
            $table->boolean('active')->default(true);
            $table->string('type', 100);
            $table->json('params')->nullable();
            $table->timestamp('last_start_at')->nullable();
            $table->timestamp('last_end_at')->nullable();
            $table->timestamps();

            $table->foreign('merchant_ext_system_id')->references('id')->on('merchant_ext_systems');
        });

        Schema::create('merchant_integration_sys_params', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_integration_id')->unsigned();
            $table->string('name');
            $table->mediumText('value');
            $table->timestamps();

            $table->foreign('merchant_integration_id')->references('id')->on('merchant_integrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_integration_sys_params');
        Schema::dropIfExists('merchant_integrations');
        Schema::dropIfExists('merchant_ext_systems');
        Schema::dropIfExists('merchant_settings');
        Schema::dropIfExists('api_users');
        Schema::dropIfExists('registration_requests');
        Schema::dropIfExists('operators');
        Schema::dropIfExists('merchants');
    }
};

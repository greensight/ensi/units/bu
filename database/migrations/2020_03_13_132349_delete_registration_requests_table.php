<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('registration_requests');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('registration_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('operator_id')->unsigned()->nullable();
            $table->bigInteger('merchant_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1);
            ;
            $table->string('operator_name');
            $table->string('operator_phone')->nullable();
            $table->string('operator_email');
            $table->text('merchant_legal_name');
            $table->text('merchant_legal_address');
            $table->string('merchant_inn', 30);
            $table->string('merchant_kpp', 30);
            $table->timestamps();

            $table->foreign('merchant_id')->references('id')->on('merchants');
            $table->foreign('operator_id')->references('id')->on('operators');
        });
    }
};

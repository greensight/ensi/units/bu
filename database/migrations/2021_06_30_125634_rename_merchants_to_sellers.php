<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('merchant_integration_sys_params', function (Blueprint $table) {
            $table->renameColumn('merchant_integration_id', 'seller_integration_id');
        });
        Schema::rename('merchant_integration_sys_params', 'seller_integration_sys_params');


        Schema::table('merchant_integrations', function (Blueprint $table) {
            $table->renameColumn('merchant_ext_system_id', 'seller_ext_system_id');
        });
        Schema::rename('merchant_integrations', 'seller_integrations');

        Schema::table('merchant_ext_systems', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });
        Schema::rename('merchant_ext_systems', 'seller_ext_systems');

        Schema::table('merchant_settings', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });
        Schema::rename('merchant_settings', 'seller_settings');

        Schema::table('api_users', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::table('operators', function (Blueprint $table) {
            $table->renameColumn('merchant_id', 'seller_id');
        });

        Schema::rename('merchants', 'sellers');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('sellers', 'merchants');

        Schema::table('operators', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::table('api_users', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::rename('seller_settings', 'merchant_settings');
        Schema::table('merchant_settings', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::rename('seller_ext_systems', 'merchant_ext_systems');
        Schema::table('merchant_ext_systems', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });

        Schema::rename('seller_integrations', 'merchant_integrations');
        Schema::table('merchant_integrations', function (Blueprint $table) {
            $table->renameColumn('seller_ext_system_id', 'merchant_ext_system_id');
        });

        Schema::rename('seller_integration_sys_params', 'merchant_integration_sys_params');
        Schema::table('merchant_integration_sys_params', function (Blueprint $table) {
            $table->renameColumn('seller_integration_id', 'merchant_integration_id');
        });

        Schema::table('stores', function (Blueprint $table) {
            $table->renameColumn('seller_id', 'merchant_id');
        });
    }
};

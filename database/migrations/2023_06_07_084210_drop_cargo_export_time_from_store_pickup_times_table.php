<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_pickup_times', function (Blueprint $table) {
            $table->dropColumn('cargo_export_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_pickup_times', function (Blueprint $table) {
            $table->time('cargo_export_time')->nullable();
        });
    }
};

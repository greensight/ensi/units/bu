<?php

namespace Tests\Feature;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class StoreWorkingsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedStoreWorkingResponseFields = [
        'id',
        'store_id',
        'active',
        'day',
        'working_start_time',
        'working_end_time',
        'created_at',
        'updated_at',
    ];

    public function testSearchStoreWorkings()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/workings:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedStoreWorkingResponseFields,
            ],
        ]);
    }

    public function testGetStoreWorking()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/stores/workings/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreWorkingResponseFields,
        ]);
    }

    public function testCreateStoreWorking()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/workings', [
            'store_id' => 1,
            'active' => true,
            'day' => 1,
            'working_start_time' => '11:00',
            'working_end_time' => '12:00',
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreWorkingResponseFields,
        ]);
    }

    public function testPatchStoreWorking()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/stores/workings/1', [
            'day' => 2,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreWorkingResponseFields,
        ]);

        $countStoreWorkings = StoreWorking::query()->where('id', 1)->where('day', 2)->count();
        $this->assertEquals(1, $countStoreWorkings);
    }

    public function testDeleteStoreWorking()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/stores/workings/1');
        $response->assertStatus(200);
        $countStoreWorkings = StoreWorking::query()->where('id', 1)->count();
        $this->assertEquals(0, $countStoreWorkings);
    }
}

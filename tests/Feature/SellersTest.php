<?php

namespace Tests\Feature;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class SellersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedSellerResponseFields = [
        'id',
        'legal_name',
        'external_id',
        'legal_address',
        'fact_address',
        'inn',
        'kpp',
        'payment_account',
        'bank',
        'bank_address',
        'bank_bik',
        'correspondent_account',
        'status',
        'status_at',
        'manager_id',
        'storage_address',
        'site',
        'can_integration',
        'sale_info',
        'city',
        'created_at',
        'updated_at',
        'operators',
        'stores',
    ];

    public function testSearchSellers()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/sellers/sellers:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedSellerResponseFields,
            ],
        ]);
    }

    public function testGetSeller()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/sellers/sellers/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedSellerResponseFields,
        ]);
    }

    public function testCreateSeller()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/sellers/sellers', [
            'legal_name' => 'test',
            'external_id' => 'test',
            'legal_address' => 'test',
            'fact_address' => 'test',
            'inn' => '1',
            'kpp' => '1',
            'payment_account' => '1',
            'bank' => '1',
            'bank_address' => 'test',
            'bank_bik' => '1',
            'correspondent_account' => '1',
            'status' => 1,
            'manager_id' => 1,
            'storage_address' => 'test',
            'site' => 'test',
            'can_integration' => false,
            'sale_info' => 'test',
            'city' => 'test',
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedSellerResponseFields,
        ]);
    }

    public function testPatchSeller()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/sellers/sellers/1', [
            'legal_name' => 'test1',
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedSellerResponseFields,
        ]);

        $countSellers = Seller::query()->where('id', 1)->where('legal_name', 'test1')->count();
        $this->assertEquals(1, $countSellers);
    }

    public function testDeleteSeller()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/sellers/sellers/1');
        $response->assertStatus(200);
        $countSellers = Seller::query()->where('id', 1)->count();
        $this->assertEquals(0, $countSellers);
    }
}

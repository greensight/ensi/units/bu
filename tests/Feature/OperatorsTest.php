<?php

namespace Tests\Feature;

use App\Domain\SellerUsers\Models\Operator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class OperatorsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedOperatorResponseFields = [
        'id',
        'seller_id',
        'user_id',
        'is_receive_sms',
        'is_main',
        'created_at',
        'updated_at',
        'seller',
    ];

    public function testSearchOperators()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/seller-users/operators:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedOperatorResponseFields,
            ],
        ]);
    }

    public function testGetOperator()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/seller-users/operators/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedOperatorResponseFields,
        ]);
    }

    public function testCreateOperator()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/seller-users/operators', [
            'seller_id' => 1,
            'user_id' => 1,
            'is_receive_sms' => true,
            'is_main' => true,
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedOperatorResponseFields,
        ]);
    }

    public function testPatchOperator()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/seller-users/operators/1', [
            'user_id' => 2,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedOperatorResponseFields,
        ]);

        $countOperators = Operator::query()->where('id', 1)->where('user_id', 2)->count();
        $this->assertEquals(1, $countOperators);
    }

    public function testDeleteOperator()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/seller-users/operators/1');
        $response->assertStatus(200);
        $countOperators = Operator::query()->where('id', 1)->count();
        $this->assertEquals(0, $countOperators);
    }
}

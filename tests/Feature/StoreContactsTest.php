<?php

namespace Tests\Feature;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class StoreContactsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedStoreContactResponseFields = [
        'id',
        'store_id',
        'name',
        'phone',
        'email',
        'created_at',
        'updated_at',
    ];

    public function testSearchStoreContacts()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/contacts:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedStoreContactResponseFields,
            ],
        ]);
    }

    public function testGetStoreContact()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/stores/contacts/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreContactResponseFields,
        ]);
    }

    public function testCreateStoreContact()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/contacts', [
            'store_id' => 1,
            'name' => 'test',
            'phone' => 'test',
            'email' => 'test',
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreContactResponseFields,
        ]);
    }

    public function testPatchStoreContact()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/stores/contacts/1', [
            'name' => 'test1',
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreContactResponseFields,
        ]);

        $countStoreContacts = StoreContact::query()->where('id', 1)->where('name', 'test1')->count();
        $this->assertEquals(1, $countStoreContacts);
    }

    public function testDeleteStoreContact()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/stores/contacts/1');
        $response->assertStatus(200);
        $countStoreContacts = StoreContact::query()->where('id', 1)->count();
        $this->assertEquals(0, $countStoreContacts);
    }
}

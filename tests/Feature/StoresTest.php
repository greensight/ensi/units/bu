<?php

namespace Tests\Feature;

use App\Domain\Stores\Models\Store;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class StoresTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedStoreResponseFields = [
        'id',
        'seller_id',
        'xml_id',
        'active',
        'name',
        'address',
        'timezone',
        'created_at',
        'updated_at',
        'workings',
        'contacts',
        'pickup_times',
    ];

    public function testSearchStores()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/stores:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedStoreResponseFields,
            ],
        ]);
    }

    public function testGetStore()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/stores/stores/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreResponseFields,
        ]);
    }

    public function testCreateStore()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/stores', [
            'seller_id' => 1,
            'xml_id' => 'test',
            'active' => true,
            'name' => 'test',
            'address' => [
                'address_string' => 'test',
                'country_code' => 'test',
                'post_index' => 123456,
                'region' => 'test',
                'region_guid' => 1,
                'city' => 'test',
                'city_guid' => 1,
                'street' => 'test',
                'house' => 1,
                'block' => 1,
                'flat' => 1,
                'porch' => 1,
                'intercom' => 'test',
                'floor' => 1,
                'comment' => 'test',
                'geo_lat' => 72.802009,
                'geo_lon' => -141.896565,
            ],
            'timezone' => 'Europe/Moscow',
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreResponseFields,
        ]);
    }

    public function testPatchStore()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/stores/stores/1', [
            'name' => 'test1',
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStoreResponseFields,
        ]);

        $countStores = Store::query()->where('id', 1)->where('name', 'test1')->count();
        $this->assertEquals(1, $countStores);
    }

    public function testDeleteStore()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/stores/stores/1');
        $response->assertStatus(200);
        $countStores = Store::query()->where('id', 1)->count();
        $this->assertEquals(0, $countStores);
    }
}

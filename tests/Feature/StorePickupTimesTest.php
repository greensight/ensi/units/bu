<?php

namespace Tests\Feature;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class StorePickupTimesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    private $expectedStorePickupTimeResponseFields = [
        'id',
        'store_id',
        'day',
        'pickup_time_code',
        'pickup_time_start',
        'pickup_time_end',
        'delivery_service',
        'created_at',
        'updated_at',
    ];

    public function testSearchStorePickupTimes()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/pickup-times:search');
        $response->assertStatus(200);
        $this->assertCount(10, $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                0 => $this->expectedStorePickupTimeResponseFields,
            ],
        ]);
    }

    public function testGetStorePickupTime()
    {
        /** @var TestResponse $response */
        $response = $this->get('/api/v1/stores/pickup-times/1');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStorePickupTimeResponseFields,
        ]);
    }

    public function testCreateStorePickupTime()
    {
        /** @var TestResponse $response */
        $response = $this->post('/api/v1/stores/pickup-times', [
            'store_id' => 1,
            'day' => 1,
            'pickup_time_code' => '11-12',
            'pickup_time_start' => '11:00',
            'pickup_time_end' => '12:00',
            'delivery_service' => 1,
        ]);
        $response->assertJsonStructure([
            'data' => $this->expectedStorePickupTimeResponseFields,
        ]);
    }

    public function testPatchStorePickupTime()
    {
        /** @var TestResponse $response */
        $response = $this->patch('/api/v1/stores/pickup-times/1', [
            'day' => 2,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => $this->expectedStorePickupTimeResponseFields,
        ]);

        $countStorePickupTimes = StorePickupTime::query()->where('id', 1)->where('day', 2)->count();
        $this->assertEquals(1, $countStorePickupTimes);
    }

    public function testDeleteStorePickupTime()
    {
        /** @var TestResponse $response */
        $response = $this->delete('/api/v1/stores/pickup-times/1');
        $response->assertStatus(200);
        $countStorePickupTimes = StorePickupTime::query()->where('id', 1)->count();
        $this->assertEquals(0, $countStorePickupTimes);
    }
}

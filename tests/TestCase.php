<?php

namespace Tests;

use App\Domain\Kafka\Actions\Send\SendKafkaMessageAction;
use Ensi\LaravelTestFactories\WithFakerProviderTestCase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFakerProviderTestCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->mockKafka();
    }

    protected function mockKafka(): void
    {
        $this->mock(SendKafkaMessageAction::class)->allows('execute');
    }
}

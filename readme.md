# Business Units

Управление внутренними и внешними юнитами

## Требования

### Требования к ПО

- docker
- composer
- npm
- git

### Общие требования Ensi

https://www.notion.so/ensibok

## Разворот локально

Разворот сервиса на локальной машине описан в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

## Зависимости

| Название | Описание  | Переменные окружения |
| --- | --- | --- |
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует** |
| Units | Ensi Admin Auth | UNITS_ADMIN_AUTH_SERVICE_HOST |

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).

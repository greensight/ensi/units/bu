SellerReadonlyProperties:
  type: object
  properties:
    id:
      type: integer
      description: Идентификатор продавца
      example: 1
    status_at:
      type: string
      format: date-time
      description: Время изменения статуса продавца
      example: "2021-01-15T14:55:35.000000Z"
    created_at:
      type: string
      format: date-time
      description: Время создания продавца
      example: "2021-01-15T14:55:35.000000Z"
    updated_at:
      type: string
      format: date-time
      description: Время обновления продавца
      example: "2021-01-15T14:55:35.000000Z"
  required:
    - id
    - status_at
    - created_at
    - updated_at

SellerFillableProperties:
  type: object
  properties:
    manager_id:
      type: integer
      description: ID менеджера
      nullable: true
      example: 1
    legal_name:
      type: string
      description: Юридическое наименование организации
      example: Сокольники
    legal_address:
      $ref: '../../common_schemas.yaml#/Address'
    fact_address:
      $ref: '../../common_schemas.yaml#/Address'
    inn:
      type: string
      description: ИНН
      nullable: true
      example: ""
    kpp:
      type: string
      description: КПП
      nullable: true
      example: ""
    payment_account:
      type: string
      description: Номер банковского счета
      nullable: true
      example: ""
    correspondent_account:
      type: string
      description: Номер корреспондентского счета банка
      nullable: true
      example: ""
    bank:
      type: string
      description: Наименование банка
      nullable: true
      example: ""
    bank_address:
      $ref: '../../common_schemas.yaml#/Address'
    bank_bik:
      type: string
      description: БИК банка
      nullable: true
      example: ""
    site:
      type: string
      description: Сайт компании
      nullable: true
      example: "https://example.com"
    info:
      type: string
      description: Бренды и товары, которыми торгует Продавец
      nullable: true
      example: ""

SellerUpdateProperties:
  type: object
  properties:
    status:
      description: Статус продавца из SellerStatusEnum
      allOf:
        - type: integer
        - $ref: '../enums/seller_status_enum.yaml'
      example: 1

SellerIncludes:
  type: object
  properties:
    operators:
      type: array
      items:
        $ref: '../../seller-users/schemas/operators.yaml#/Operator'
    stores:
      type: array
      items:
        $ref: '../../stores/schemas/stores.yaml#/Store'

SellerFillableRequiredProperties:
  required:
    - manager_id
    - legal_name
    - legal_address
    - fact_address
    - inn
    - kpp
    - payment_account
    - correspondent_account
    - bank
    - bank_address
    - bank_bik
    - site
    - info

SellerUpdateRequiredProperties:
  required:
    - status

Seller:
  allOf:
    - $ref: '#/SellerReadonlyProperties'
    - $ref: '#/SellerFillableProperties'
    - $ref: '#/SellerFillableRequiredProperties'
    - $ref: '#/SellerUpdateProperties'
    - $ref: '#/SellerUpdateRequiredProperties'
    - $ref: '#/SellerIncludes'

CreateSellerRequest:
  allOf:
    - $ref: '#/SellerFillableProperties'
    - $ref: '#/SellerFillableRequiredProperties'

PatchSellerRequest:
  allOf:
    - $ref: '#/SellerFillableProperties'
    - $ref: '#/SellerUpdateProperties'

SearchSellersRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      type: object
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

RegisterSellerRequest:
  allOf:
    - $ref: '#/CreateWithOperatorSellerRequest'

CreateWithOperatorSellerRequest:
  type: object
  properties:
    seller:
      $ref: '#/CreateSellerRequest'
    operator:
      type: object
      properties:
        first_name:
          type: string
          description: Имя оператора
          example: Иван
        middle_name:
          type: string
          description: Отчество оператора
          example: Иванович
        last_name:
          type: string
          description: Фамилия оператора
          example: Иванов
        email:
          type: string
          description: Email
          example: ivanov@mail.com
        phone:
          type: string
          description: Телефон
          example: "+79999999999"
        password:
          type: string
          description: Пароль
          example: test1234

SellerResponse:
  type: object
  properties:
    data:
      $ref: '#/Seller'
    meta:
      type: object
  required:
    - data

SearchSellersResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/Seller'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

<?php

namespace App\Domain\Stores\Observers;

use App\Domain\Kafka\Actions\Send\SendStoreEventAction;
use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Stores\Models\Store;

class StoreAfterCommitObserver
{
    public bool $afterCommit = true;

    public function __construct(protected readonly SendStoreEventAction $sendStoreEventAction)
    {
    }

    public function created(Store $store): void
    {
        $this->sendStoreEventAction->execute($store, ModelEventMessage::CREATE);
    }

    public function updated(Store $store): void
    {
        $this->sendStoreEventAction->execute($store, ModelEventMessage::UPDATE);
    }
}

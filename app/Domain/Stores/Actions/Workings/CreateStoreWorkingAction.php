<?php

namespace App\Domain\Stores\Actions\Workings;

use App\Domain\Sellers\Actions\SellerVerificationAction;
use App\Domain\Stores\Models\StoreWorking;
use App\Exceptions\ValidateException;

class CreateStoreWorkingAction
{
    public function __construct(protected readonly SellerVerificationAction $sellerVerificationAction)
    {
    }

    public function execute(array $fields): StoreWorking
    {
        $storeWorking = new StoreWorking();
        $storeWorking->fill($fields);

        $sellerId = data_get($fields, 'seller_id');
        $storeId = data_get($fields, 'store_id');
        if (!$this->sellerVerificationAction->execute($storeWorking, $sellerId)) {
            throw new ValidateException("У продавца {$sellerId} нет склада {$storeId}");
        }

        $storeWorking->save();

        return $storeWorking;
    }
}

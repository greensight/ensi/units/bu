<?php

namespace App\Domain\Stores\Actions\Workings;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Database\Eloquent\Builder;
use Webmozart\Assert\Assert;

class DeleteStoreWorkingAction
{
    public function execute(int $id, ?Builder $query = null): void
    {
        $query = $query ?: StoreWorking::query();
        Assert::isAOf($query->getModel(), StoreWorking::class);

        $storeWorking = $query->findOrFail($id);
        $storeWorking->delete();
    }
}

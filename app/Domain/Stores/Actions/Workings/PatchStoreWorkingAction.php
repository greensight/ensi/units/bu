<?php

namespace App\Domain\Stores\Actions\Workings;

use App\Domain\Stores\Models\StoreWorking;

class PatchStoreWorkingAction
{
    public function execute(StoreWorking $storeWorking, array $fields): StoreWorking
    {
        $storeWorking->update($fields);

        return $storeWorking;
    }
}

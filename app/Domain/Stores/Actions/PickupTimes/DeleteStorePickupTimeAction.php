<?php

namespace App\Domain\Stores\Actions\PickupTimes;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Database\Eloquent\Builder;
use Webmozart\Assert\Assert;

class DeleteStorePickupTimeAction
{
    public function execute(int $id, ?Builder $query = null): void
    {
        $query = $query ?: StorePickupTime::query();
        Assert::isAOf($query->getModel(), StorePickupTime::class);

        $storePickupTime = $query->findOrFail($id);
        $storePickupTime->delete();
    }
}

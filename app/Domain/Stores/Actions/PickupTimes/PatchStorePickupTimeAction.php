<?php

namespace App\Domain\Stores\Actions\PickupTimes;

use App\Domain\Stores\Models\StorePickupTime;

class PatchStorePickupTimeAction
{
    public function execute(StorePickupTime $storePickupTime, array $fields): StorePickupTime
    {
        $storePickupTime->update($fields);

        return $storePickupTime;
    }
}

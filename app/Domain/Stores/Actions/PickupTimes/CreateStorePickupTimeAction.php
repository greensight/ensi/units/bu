<?php

namespace App\Domain\Stores\Actions\PickupTimes;

use App\Domain\Sellers\Actions\SellerVerificationAction;
use App\Domain\Stores\Models\StorePickupTime;
use App\Exceptions\ValidateException;

class CreateStorePickupTimeAction
{
    public function __construct(protected readonly SellerVerificationAction $sellerVerificationAction)
    {
    }

    public function execute(array $fields): StorePickupTime
    {
        $storePickupTime = new StorePickupTime();
        $storePickupTime->fill($fields);

        $sellerId = data_get($fields, 'seller_id');
        $storeId = data_get($fields, 'store_id');
        if (!$this->sellerVerificationAction->execute($storePickupTime, $sellerId)) {
            throw new ValidateException("У продавца {$sellerId} нет склада {$storeId}");
        }

        $storePickupTime->save();

        return $storePickupTime;
    }
}

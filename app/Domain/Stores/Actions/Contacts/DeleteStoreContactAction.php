<?php

namespace App\Domain\Stores\Actions\Contacts;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Database\Eloquent\Builder;
use Webmozart\Assert\Assert;

class DeleteStoreContactAction
{
    public function execute(int $id, ?Builder $query = null): void
    {
        $query = $query ?: StoreContact::query();
        Assert::isAOf($query->getModel(), StoreContact::class);

        $storeContact = $query->findOrFail($id);
        $storeContact->delete();
    }
}

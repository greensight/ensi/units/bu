<?php

namespace App\Domain\Stores\Actions\Contacts;

use App\Domain\Stores\Models\StoreContact;

class PatchStoreContactAction
{
    public function execute(StoreContact $storeContact, array $fields): StoreContact
    {
        $storeContact->update($fields);

        return $storeContact;
    }
}

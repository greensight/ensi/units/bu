<?php

namespace App\Domain\Stores\Actions\Contacts;

use App\Domain\Sellers\Actions\SellerVerificationAction;
use App\Domain\Stores\Models\StoreContact;
use App\Exceptions\ValidateException;

class CreateStoreContactAction
{
    public function __construct(protected readonly SellerVerificationAction $sellerVerificationAction)
    {
    }

    public function execute(array $fields): StoreContact
    {
        $storeContact = new StoreContact();
        $storeContact->fill($fields);

        $sellerId = data_get($fields, 'seller_id');
        $storeId = data_get($fields, 'store_id');
        if (!$this->sellerVerificationAction->execute($storeContact, $sellerId)) {
            throw new ValidateException("У продавца {$sellerId} нет склада {$storeId}");
        }

        $storeContact->save();

        return $storeContact;
    }
}

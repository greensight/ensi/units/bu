<?php

namespace App\Domain\Stores\Actions\Stores;

use App\Domain\Stores\Models\Store;

class CreateStoreAction
{
    public function execute(array $fields): Store
    {
        $store = new Store();
        $store->fill($fields);
        $store->save();

        return $store;
    }
}

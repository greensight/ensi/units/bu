<?php

namespace App\Domain\Stores\Actions\Stores;

use App\Domain\Stores\Models\Store;

class PatchStoreAction
{
    public function execute(Store $store, array $fields): Store
    {
        $store->update($fields);

        return $store;
    }
}

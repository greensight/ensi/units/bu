<?php

namespace App\Domain\Stores\Models\Tests\Factories;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StorePickupTime;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<StorePickupTime>
 */
class StorePickupTimeFactory extends BaseModelFactory
{
    protected $model = StorePickupTime::class;

    public function definition(): array
    {
        return [
            'store_id' => Store::factory(),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => $this->faker->nullable()->exactly(
                "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}"
            ),
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'delivery_service' => null,
        ];
    }
}

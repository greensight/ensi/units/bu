<?php

namespace App\Domain\Stores\Models\Tests\Factories;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreContact;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<StoreContact>
 */
class StoreContactFactory extends BaseModelFactory
{
    protected $model = StoreContact::class;

    public function definition(): array
    {
        return [
            'store_id' => Store::factory(),
            'name' => $this->faker->nullable()->name(),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'email' => $this->faker->nullable()->email(),
        ];
    }
}

<?php

namespace App\Domain\Stores\Models\Tests\Factories;

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Store>
 */
class StoreFactory extends BaseModelFactory
{
    protected $model = Store::class;

    public function definition(): array
    {
        return [
            'seller_id' => $this->faker->nullable()->exactly(Seller::factory()),
            'xml_id' => $this->faker->nullable()->exactly($this->faker->unique()->word()),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => $this->faker->nullable()->exactly(AddressData::factory()->make()),
            'timezone' => $this->faker->timezone(),
        ];
    }
}

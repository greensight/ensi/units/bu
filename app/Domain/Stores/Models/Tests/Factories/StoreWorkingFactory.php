<?php

namespace App\Domain\Stores\Models\Tests\Factories;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreWorking;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<StoreWorking>
 */
class StoreWorkingFactory extends BaseModelFactory
{
    protected $model = StoreWorking::class;

    public function definition(): array
    {
        return [
            'store_id' => Store::factory(),
            'active' => $this->faker->boolean,
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->nullable()->time('H:i'),
            'working_end_time' => $this->faker->nullable()->time('H:i'),
        ];
    }
}

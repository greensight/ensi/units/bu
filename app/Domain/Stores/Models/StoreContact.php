<?php

namespace App\Domain\Stores\Models;

use App\Domain\Sellers\Contracts\SellerEntity;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Tests\Factories\StoreContactFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Склады - контакты"
 * Class StoreContact
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property string|null $name - имя
 * @property string|null $phone - телефон
 * @property string|null $email - email
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Store $store
 */
class StoreContact extends Model implements SellerEntity
{
    protected $table = 'store_contacts';

    protected $fillable = ['store_id', 'name', 'phone', 'email'];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }

    public function getSeller(): ?Seller
    {
        /** @var Seller|null $seller */
        $seller = Seller::query()
            ->whereHas(
                'stores',
                fn (Builder $relationQuery) => $relationQuery->where('id', $this->store_id)
            )
            ->first();

        return $seller;
    }

    public static function factory(): StoreContactFactory
    {
        return StoreContactFactory::new();
    }
}

<?php

namespace App\Domain\Stores\Models;

use App\Domain\Sellers\Contracts\SellerEntity;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Tests\Factories\StorePickupTimeFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Склады - время отгрузки грузов"
 * Class StorePickupTime
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property int $day - день недели (1-7)
 * @property string|null $pickup_time_code - код времени отгрузки у службы доставки
 * @property string $pickup_time_start - время начала отгрузки
 * @property string $pickup_time_end - время окончания отгрузки
 * @property int|null $delivery_service - служба доставки (если указана, то данная информация переопределяет данные дня недели без службы доставки) PS: завязана с логистикой, пока не знаю нужна ли - оставляем
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Store $store
 */
class StorePickupTime extends Model implements SellerEntity
{
    protected $table = 'store_pickup_times';

    protected $fillable = [
        'store_id',
        'day',
        'pickup_time_code',
        'pickup_time_start',
        'pickup_time_end',
        'delivery_service',
    ];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }

    public function getSeller(): ?Seller
    {
        /** @var Seller|null $seller */
        $seller = Seller::query()
            ->whereHas(
                'stores',
                fn (Builder $relationQuery) => $relationQuery->where('id', $this->store_id)
            )
            ->first();

        return $seller;
    }

    public static function factory(): StorePickupTimeFactory
    {
        return StorePickupTimeFactory::new();
    }
}

<?php

namespace App\Domain\Stores\Models;

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Tests\Factories\StoreFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Класс-модель для сущности "Склады"
 * Class Store
 *
 * @property int $id - ID склада
 * @property int|null $seller_id - id продавца
 * @property string|null $xml_id - id склада у продавца
 * @property bool $active - флаг активности склада
 * @property string $name - название
 * @property AddressData|null $address - адрес
 * @property string $timezone - часовой пояс
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Seller $seller
 * @property-read Collection|StoreWorking[] $storeWorking
 * @property-read Collection|StoreContact[] $storeContact
 * @property-read Collection|StorePickupTime[] $storePickupTime
 */
class Store extends Model
{
    protected $table = 'stores';

    protected $fillable = ['seller_id', 'xml_id', 'active', 'name', 'address', 'timezone'];

    protected $casts = [
        'seller_id' => 'int',
        'active' => 'bool',
        'address' => AddressData::class,
    ];

    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    public function workings(): HasMany
    {
        return $this->hasMany(StoreWorking::class, 'store_id');
    }

    public function contacts(): HasMany
    {
        return $this->hasMany(StoreContact::class, 'store_id');
    }

    public function contact(): HasOne
    {
        return $this->hasOne(StoreContact::class, 'store_id')->oldestOfMany();
    }

    public function pickupTimes(): HasMany
    {
        return $this->hasMany(StorePickupTime::class, 'store_id');
    }

    public static function factory(): StoreFactory
    {
        return StoreFactory::new();
    }
}

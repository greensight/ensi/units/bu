<?php

namespace App\Domain\Stores\Models;

use App\Domain\Sellers\Contracts\SellerEntity;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Tests\Factories\StoreWorkingFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Склады - время работы"
 * Class StoreWorking
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property bool $active - флаг активности дня работы склада
 * @property int $day - день недели (1-7)
 * @property string|null $working_start_time - время начала работы склада (00:00)
 * @property string|null $working_end_time - время конца работы склада (00:00)
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Store $store
 */
class StoreWorking extends Model implements SellerEntity
{
    protected $table = 'store_workings';

    protected $fillable = [
        'store_id',
        'active',
        'day',
        'working_start_time',
        'working_end_time',
    ];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class);
    }

    public function getSeller(): ?Seller
    {
        /** @var Seller $seller|null */
        $seller = Seller::query()
            ->whereHas(
                'stores',
                fn (Builder $relationQuery) => $relationQuery->where('id', $this->store_id)
            )
            ->first();

        return $seller;
    }

    public static function factory(): StoreWorkingFactory
    {
        return StoreWorkingFactory::new();
    }
}

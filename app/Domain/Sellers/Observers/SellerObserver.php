<?php

namespace App\Domain\Sellers\Observers;

use App\Domain\Sellers\Models\Seller;

class SellerObserver
{
    public function saving(Seller $seller): void
    {
        if (!$seller->exists || $seller->isDirty('status')) {
            $seller->status_at = now();
        }
    }
}

<?php

namespace App\Domain\Sellers\Models;

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Tests\Factories\SellerFactory;
use App\Domain\SellerUsers\Models\Operator;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Класс-модель для сущности "Продавец"
 *
 * @property int $id - Идентификатор продавца
 * @property int|null $manager_id - ид менеджера
 * @property string $legal_name - Юридическое наименование организации
 * @property AddressData|null $legal_address - Юридический адрес
 * @property AddressData|null $fact_address Фактический адрес
 * @property string|null $inn - ИНН
 * @property string|null $kpp - КПП
 * @property string|null $payment_account - Номер расчетного счета
 * @property string|null $correspondent_account - Номер корреспондентского счета банка
 * @property string|null $bank - Наименование банка
 * @property AddressData|null $bank_address - юридический адрес банка
 * @property string|null $bank_bik - БИК банка
 * @property SellerStatusEnum $status - статус
 * @property CarbonInterface $status_at - дата смены статуса
 * @property string|null $site - Сайт компании
 * @property string|null $info - Бренды и товары, которыми торгует Продавец
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Collection<Operator>|null $operators - менеджеры продавца
 * @property-read Collection<Store>|null $stores - склады
 */
class Seller extends Model
{
    protected $table = 'sellers';

    protected $fillable = [
        'manager_id', 'legal_name', 'legal_address', 'fact_address', 'inn', 'kpp', 'payment_account',
        'correspondent_account', 'bank', 'bank_address', 'bank_bik', 'status', 'site', 'info',
    ];

    protected $casts = [
        'manager_id' => 'int',
        'status' => SellerStatusEnum::class,
        'status_at' => 'datetime',
        'legal_address' => AddressData::class,
        'fact_address' => AddressData::class,
        'bank_address' => AddressData::class,
    ];

    protected $attributes = [
        'status' => SellerStatusEnum::CREATE,
    ];

    public function operators(): HasMany
    {
        return $this->hasMany(Operator::class);
    }

    public function stores(): HasMany
    {
        return $this->hasMany(Store::class);
    }

    public static function factory(): SellerFactory
    {
        return SellerFactory::new();
    }
}

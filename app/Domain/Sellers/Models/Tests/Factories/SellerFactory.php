<?php

namespace App\Domain\Sellers\Models\Tests\Factories;

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Seller>
 */
class SellerFactory extends BaseModelFactory
{
    protected $model = Seller::class;

    public function definition(): array
    {
        return [
            'manager_id' => $this->faker->nullable()->modelId(),
            'legal_name' => $this->faker->unique()->company(),
            'legal_address' => $this->faker->nullable()->exactly(AddressData::factory()->make()),
            'fact_address' => $this->faker->nullable()->exactly(AddressData::factory()->make()),
            'inn' => $this->faker->nullable()->numerify('##########'),
            'kpp' => $this->faker->nullable()->numerify('#########'),
            'payment_account' => $this->faker->nullable()->numerify('####################'),
            'correspondent_account' => $this->faker->nullable()->numerify('####################'),
            'bank' => $this->faker->nullable()->company(),
            'bank_address' => $this->faker->nullable()->exactly(AddressData::factory()->make()),
            'bank_bik' => $this->faker->nullable()->numerify('#########'),
            'status' => $this->faker->randomEnum(SellerStatusEnum::cases()),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->nullable()->text(),
        ];
    }
}

<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;

class CreateWithOperatorSellerAction
{
    public function __construct(protected readonly NewSellerWithOperatorAction $newSellerWithOperatorAction)
    {
    }

    public function execute(array $fields): Seller
    {
        return $this->newSellerWithOperatorAction->execute($fields, SellerStatusEnum::ACTIVE);
    }
}

<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Contracts\SellerEntity;

class SellerVerificationAction
{
    public function execute(SellerEntity $entity, ?int $sellerId): bool
    {
        if ($sellerId == null) {
            return true;
        }

        return $entity->getSeller()?->id === $sellerId;
    }
}

<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use App\Domain\SellerUsers\Actions\CreateOperatorAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\AddRolesToUserRequest;
use Ensi\AdminAuthClient\Dto\CreateUserRequest;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Ensi\AdminAuthClient\Dto\User;
use Exception;
use Illuminate\Support\Facades\DB;

class NewSellerWithOperatorAction
{
    public function __construct(
        private UsersApi $usersApi,
        private CreateSellerAction $createSellerAction,
        private CreateOperatorAction $createOperatorAction,
    ) {
    }

    public function execute(array $fields, SellerStatusEnum $status): Seller
    {
        $sellerData = data_get($fields, 'seller');
        $operatorUserData = data_get($fields, 'operator');

        $userId = $this->createUser($operatorUserData)->getId();

        $seller = DB::transaction(function () use ($sellerData, $status, $userId) {
            $sellerData['status'] = $status;
            $seller = $this->createSellerAction->execute($sellerData);

            $this->createOperatorAction->execute([
                'seller_id' => $seller->id,
                'user_id' => $userId,
                'is_main' => true,
            ]);

            return $seller;
        });

        return $seller->load('operators');
    }

    protected function createUser(array $userData): User
    {
        if (!isset($userData['email'])) {
            throw new Exception('Для создания продавца необходимо указать email оператора');
        }

        $createUserRequest = new CreateUserRequest($userData);
        $createUserRequest->setLogin($userData['email']);
        $user = $this->usersApi->createUser($createUserRequest)->getData();

        $rolesRequest = new AddRolesToUserRequest(['roles' => [RoleEnum::SELLER_ADMINISTRATOR]]);
        $this->usersApi->addRolesToUser($user->getId(), $rolesRequest);

        return $user;
    }
}

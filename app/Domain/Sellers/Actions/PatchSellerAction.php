<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Support\Facades\DB;

class PatchSellerAction
{
    public function execute(int $id, array $fields): Seller
    {
        return DB::transaction(function () use ($id, $fields) {
            /** @var Seller $seller */
            $seller = Seller::query()->findOrFail($id);
            $seller->fill($fields);
            $seller->save();

            return $seller;
        });
    }
}

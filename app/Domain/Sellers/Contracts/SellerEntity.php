<?php

namespace App\Domain\Sellers\Contracts;

use App\Domain\Sellers\Models\Seller;

interface SellerEntity
{
    public function getSeller(): ?Seller;
}

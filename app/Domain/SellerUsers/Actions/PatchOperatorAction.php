<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;

class PatchOperatorAction
{
    public function execute(int $id, array $fields): Operator
    {
        /** @var Operator $operator */
        $operator = Operator::query()->findOrFail($id);
        $operator->update($fields);

        return $operator;
    }
}

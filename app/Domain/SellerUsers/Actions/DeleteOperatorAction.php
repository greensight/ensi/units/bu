<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;

class DeleteOperatorAction
{
    public function execute(int $id): void
    {
        Operator::destroy($id);
    }
}

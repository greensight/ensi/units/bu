<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;

class CreateOperatorAction
{
    public function execute(array $fields): Operator
    {
        $operator = new Operator();
        $operator->fill($fields);
        $operator->save();

        return $operator;
    }
}

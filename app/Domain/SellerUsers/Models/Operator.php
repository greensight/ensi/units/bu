<?php

namespace App\Domain\SellerUsers\Models;

use App\Domain\Sellers\Models\Seller;
use App\Domain\SellerUsers\Models\Tests\Factories\OperatorFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Менеджер продавца"
 *
 * @property int $id - идентификатор оператора
 * @property int $seller_id
 * @property int $user_id
 * @property bool $is_receive_sms
 * @property bool $is_main - главное контактное лицо продавца
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read Seller $seller - продавец
 */
class Operator extends Model
{
    protected $table = 'operators';

    protected $fillable = ['seller_id', 'user_id', 'is_receive_sms', 'is_main'];

    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    public static function factory(): OperatorFactory
    {
        return OperatorFactory::new();
    }
}

<?php

namespace App\Domain\SellerUsers\Models\Tests\Factories;

use App\Domain\Sellers\Models\Seller;
use App\Domain\SellerUsers\Models\Operator;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Operator>
 */
class OperatorFactory extends BaseModelFactory
{
    protected $model = Operator::class;

    public function definition(): array
    {
        return [
            'seller_id' => Seller::factory(),
            'user_id' => $this->faker->modelId(),
            'is_receive_sms' => $this->faker->boolean,
            'is_main' => $this->faker->boolean,
        ];
    }
}

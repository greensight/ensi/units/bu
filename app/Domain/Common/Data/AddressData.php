<?php

namespace App\Domain\Common\Data;

use App\Domain\Common\Data\Tests\Factories\AddressDataFactory;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Fluent;

/**
 * @property string|null $address_string - Адрес полностью
 * @property string|null $country_code - Код страны
 * @property string|null $post_index - Почтовые индекс
 * @property string|null $region - Регион
 * @property string|null $region_guid - GUID региона
 * @property string|null $area - Область
 * @property string|null $area_guid - GUID области
 * @property string|null $city - Город
 * @property string|null $city_guid - Код города
 * @property string|null $street - Улица
 * @property string|null $house - Дом
 * @property string|null $block - Корпус
 * @property string|null $flat - Квартира
 * @property string|null $floor - Этаж
 * @property string|null $porch - Подъезд
 * @property string|null $intercom - Домофон
 * @property string|null $geo_lat - Широта
 * @property string|null $geo_lon - Долгота
 */
class AddressData extends Fluent implements Castable
{
    public static function rules(): array
    {
        return [
            "address_string" => ['nullable', 'string'],
            "country_code" => ['nullable', 'string'],
            "post_index" => ['nullable', 'string'],
            "region" => ['nullable', 'string'],
            "region_guid" => ['nullable', 'string'],
            "area" => ['nullable', 'string'],
            "area_guid" => ['nullable', 'string'],
            "city" => ['nullable', 'string'],
            "city_guid" => ['nullable', 'string'],
            "street" => ['nullable', 'string'],
            "house" => ['nullable', 'string'],
            "block" => ['nullable', 'string'],
            "flat" => ['nullable', 'string'],
            "floor" => ['nullable', 'string'],
            "porch" => ['nullable', 'string'],
            "intercom" => ['nullable', 'string'],
            "geo_lat" => ['nullable', 'string'],
            "geo_lon" => ['nullable', 'string'],
        ];
    }

    public function toArray(): array
    {
        return array_intersect_key($this->attributes, self::rules());
    }

    public static function factory(): AddressDataFactory
    {
        return AddressDataFactory::new();
    }

    public static function castUsing(array $arguments): CastsAttributes
    {
        return new class () implements CastsAttributes {
            public function get($model, string $key, mixed $value, array $attributes): ?AddressData
            {
                return isset($attributes[$key]) ? new AddressData(json_decode($attributes[$key])) : null;
            }

            public function set($model, string $key, mixed $value, array $attributes): ?string
            {
                return match (true) {
                    is_array($value) => (new AddressData($value))->toJson(),
                    $value instanceof AddressData => $value->toJson(),
                    default => null,
                };
            }
        };
    }
}

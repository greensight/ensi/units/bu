<?php

namespace App\Domain\Common\Data\Tests\Factories;

use App\Domain\Common\Data\AddressData;
use Ensi\LaravelTestFactories\Factory;

class AddressDataFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'address_string' => $this->faker->address(),
            'country_code' => $this->faker->nullable()->countryCode(),
            'post_index' => $this->faker->nullable()->postcode(),
            'region' => $this->faker->nullable()->city(),
            'region_guid' => $this->faker->nullable()->uuid(),
            'area' => $this->faker->nullable()->city(),
            'area_guid' => $this->faker->nullable()->uuid(),
            'city' => $this->faker->nullable()->city(),
            'city_guid' => $this->faker->nullable()->uuid(),
            'street' => $this->faker->nullable()->streetAddress(),
            'house' => $this->faker->nullable()->buildingNumber(),
            'block' => $this->faker->nullable()->numerify('##'),
            'flat' => $this->faker->nullable()->numerify('##'),
            'floor' => $this->faker->nullable()->numerify('##'),
            'porch' => $this->faker->nullable()->numerify('##'),
            'intercom' => $this->faker->nullable()->numerify('##-##'),
            'geo_lat' => (string)$this->faker->nullable()->latitude(),
            'geo_lon' => (string)$this->faker->nullable()->longitude(),
        ];
    }

    public function make(array $extra = []): AddressData
    {
        return new AddressData($this->makeArray($extra));
    }
}

<?php

namespace App\Domain\Kafka\Messages\Send\ModelEvent;

use App\Domain\Kafka\Messages\Send\Payload;
use App\Domain\Stores\Models\Store;

class StorePayload extends Payload
{
    public function __construct(protected Store $model)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->model->id,
            'seller_id' => $this->model->seller_id,
            'xml_id' => $this->model->xml_id,
            'active' => $this->model->active,

            'name' => $this->model->name,
            'address' => $this->model->address?->toArray(),
            'timezone' => $this->model->timezone,

            'created_at' => $this->model->created_at?->toJSON(),
            'updated_at' => $this->model->updated_at?->toJSON(),
        ];
    }
}

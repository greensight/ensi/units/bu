<?php

use App\Domain\Kafka\Messages\Send\ModelEvent\StorePayload;
use App\Domain\Stores\Models\Store;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-send-messages');

test("generate StorePayload success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Store::factory()->create();

    $payload = new StorePayload($model);

    assertEquals($payload->jsonSerialize(), $model->attributesToArray());
})->with(FakerProvider::$optionalDataset);

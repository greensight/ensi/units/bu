<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\KafkaMessage;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;

class SendKafkaMessageAction
{
    public function execute(KafkaMessage $message): void
    {
        (new HighLevelProducer($message->topicKey()))->sendOne(json_encode($message->toArray()));
    }
}

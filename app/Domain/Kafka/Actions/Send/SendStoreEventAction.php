<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Send\ModelEvent\StorePayload;
use App\Domain\Stores\Models\Store;

class SendStoreEventAction
{
    public function __construct(protected readonly SendKafkaMessageAction $sendAction)
    {
    }

    public function execute(Store $model, string $event): void
    {
        $modelEvent = new ModelEventMessage($model, new StorePayload($model), $event, 'stores');
        $this->sendAction->execute($modelEvent);
    }
}

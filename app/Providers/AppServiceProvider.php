<?php

namespace App\Providers;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Sellers\Observers\SellerObserver;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\ServiceProvider;
use Spatie\QueryBuilder\QueryBuilderRequest;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
    }

    public function boot(): void
    {
        Model::preventLazyLoading(!app()->isProduction());
        Date::use(CarbonImmutable::class);
        QueryBuilderRequest::setFilterArrayValueDelimiter('');

        $this->addObservers();
    }

    protected function addObservers(): void
    {
        Seller::observe(SellerObserver::class);
    }
}

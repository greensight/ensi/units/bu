<?php

namespace App\Providers;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Observers\StoreAfterCommitObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    public function boot(): void
    {
        Store::observe(StoreAfterCommitObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}

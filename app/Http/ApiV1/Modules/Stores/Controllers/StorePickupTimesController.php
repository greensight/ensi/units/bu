<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\PickupTimes\CreateStorePickupTimeAction;
use App\Domain\Stores\Actions\PickupTimes\DeleteStorePickupTimeAction;
use App\Domain\Stores\Actions\PickupTimes\PatchStorePickupTimeAction;
use App\Domain\Stores\Models\StorePickupTime;
use App\Http\ApiV1\Modules\Stores\Queries\StorePickupTimesQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Stores\Requests\ExtendedPatchStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StorePickupTimesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StorePickupTimesController
{
    public function create(CreateStorePickupTimeRequest $request, CreateStorePickupTimeAction $action): Responsable
    {
        return new StorePickupTimesResource($action->execute($request->validated()));
    }

    public function get(int $id, StorePickupTimesQuery $query): Responsable
    {
        return new StorePickupTimesResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchStorePickupTimeRequest $request, PatchStorePickupTimeAction $action): Responsable
    {
        /** @var StorePickupTime $storePickupTime */
        $storePickupTime = StorePickupTime::query()->findOrFail($id);

        return new StorePickupTimesResource($action->execute($storePickupTime, $request->validated()));
    }

    public function extendedPatch(int $id, StorePickupTimesQuery $query, ExtendedPatchStorePickupTimeRequest $request, PatchStorePickupTimeAction $action): Responsable
    {
        /** @var StorePickupTime $storePickupTime */
        $storePickupTime = $query->findOrFail($id);

        return new StorePickupTimesResource($action->execute($storePickupTime, $request->getFields()));
    }

    public function delete(int $id, StorePickupTimesQuery $query, DeleteStorePickupTimeAction $action): Responsable
    {
        $action->execute($id, $query->getEloquentBuilder());

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StorePickupTimesQuery $query): Responsable
    {
        return StorePickupTimesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}

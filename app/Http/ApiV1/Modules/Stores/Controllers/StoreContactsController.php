<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\Contacts\CreateStoreContactAction;
use App\Domain\Stores\Actions\Contacts\DeleteStoreContactAction;
use App\Domain\Stores\Actions\Contacts\PatchStoreContactAction;
use App\Domain\Stores\Models\StoreContact;
use App\Http\ApiV1\Modules\Stores\Queries\StoreContactsQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateStoreContactRequest;
use App\Http\ApiV1\Modules\Stores\Requests\ExtendedPatchStoreContactRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreContactRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoreContactsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StoreContactsController
{
    public function create(CreateStoreContactRequest $request, CreateStoreContactAction $action): Responsable
    {
        return new StoreContactsResource($action->execute($request->validated()));
    }

    public function get(int $id, StoreContactsQuery $query): Responsable
    {
        return new StoreContactsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchStoreContactRequest $request, PatchStoreContactAction $action): Responsable
    {
        /** @var StoreContact $storeContact */
        $storeContact = StoreContact::query()->findOrFail($id);

        return new StoreContactsResource($action->execute($storeContact, $request->validated()));
    }

    public function extendedPatch(int $id, ExtendedPatchStoreContactRequest $request, StoreContactsQuery $query, PatchStoreContactAction $action): Responsable
    {
        /** @var StoreContact $storeContact */
        $storeContact = $query->findOrFail($id);

        return new StoreContactsResource($action->execute($storeContact, $request->getFields()));
    }

    public function delete(int $id, StoreContactsQuery $query, DeleteStoreContactAction $action): Responsable
    {
        $action->execute($id, $query->getEloquentBuilder());

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoreContactsQuery $query): Responsable
    {
        return StoreContactsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}

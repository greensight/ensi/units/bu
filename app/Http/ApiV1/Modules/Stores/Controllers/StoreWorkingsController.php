<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\Workings\CreateStoreWorkingAction;
use App\Domain\Stores\Actions\Workings\DeleteStoreWorkingAction;
use App\Domain\Stores\Actions\Workings\PatchStoreWorkingAction;
use App\Domain\Stores\Models\StoreWorking;
use App\Http\ApiV1\Modules\Stores\Queries\StoreWorkingsQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateStoreWorkingRequest;
use App\Http\ApiV1\Modules\Stores\Requests\ExtendedPatchStoreWorkingRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreWorkingRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoreWorkingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StoreWorkingsController
{
    public function create(CreateStoreWorkingRequest $request, CreateStoreWorkingAction $action): Responsable
    {
        return new StoreWorkingsResource($action->execute($request->validated()));
    }

    public function get(int $id, StoreWorkingsQuery $query): Responsable
    {
        return new StoreWorkingsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchStoreWorkingRequest $request, PatchStoreWorkingAction $action): Responsable
    {
        /** @var StoreWorking $storeWorking */
        $storeWorking = StoreWorking::query()->findOrFail($id);

        return new StoreWorkingsResource($action->execute($storeWorking, $request->validated()));
    }

    public function extendedPatch(int $id, StoreWorkingsQuery $query, ExtendedPatchStoreWorkingRequest $request, PatchStoreWorkingAction $action): Responsable
    {
        /** @var StoreWorking $storeWorking */
        $storeWorking = $query->findOrFail($id);

        return new StoreWorkingsResource($action->execute($storeWorking, $request->getFields()));
    }

    public function delete(int $id, StoreWorkingsQuery $query, DeleteStoreWorkingAction $action): EmptyResource
    {
        $action->execute($id, $query->getEloquentBuilder());

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoreWorkingsQuery $query): Responsable
    {
        return StoreWorkingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}

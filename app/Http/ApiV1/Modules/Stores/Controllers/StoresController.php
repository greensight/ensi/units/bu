<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\Stores\CreateStoreAction;
use App\Domain\Stores\Actions\Stores\PatchStoreAction;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Modules\Stores\Queries\StoresQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateStoreRequest;
use App\Http\ApiV1\Modules\Stores\Requests\ExtendedPatchStoreRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoresResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class StoresController
{
    public function create(CreateStoreRequest $request, CreateStoreAction $action): Responsable
    {
        return StoresResource::make($action->execute($request->validated()));
    }

    public function get(int $id, StoresQuery $query): Responsable
    {
        return StoresResource::make($query->findOrFail($id));
    }

    public function patch(int $id, PatchStoreRequest $request, PatchStoreAction $action): Responsable
    {
        /** @var Store $store */
        $store = Store::query()->findOrFail($id);

        return StoresResource::make($action->execute($store, $request->validated()));
    }

    public function extendedPatch(int $id, StoresQuery $query, ExtendedPatchStoreRequest $request, PatchStoreAction $action): Responsable
    {
        /** @var Store $store */
        $store = $query->findOrFail($id);

        return StoresResource::make($action->execute($store, $request->getFields()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoresQuery $query): Responsable
    {
        return StoresResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(StoresQuery $query): Responsable
    {
        return new StoresResource($query->firstOrFail());
    }
}

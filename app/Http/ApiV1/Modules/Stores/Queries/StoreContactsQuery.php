<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\StoreContact;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StoreContactsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(StoreContact::query());

        $this->allowedSorts([
            'id',
            'store_id',
            'name',
            'phone',
            'email',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::callback('seller_id', function (Builder $query, $value) {
                $query->whereHas('store', fn (Builder $relationQuery) => $relationQuery->where('seller_id', $value));
            }),
            ...NumericFilter::make('id')->exact(),
            ...NumericFilter::make('store_id')->exact(),
            ...StringFilter::make('name')->exact(),
            ...StringFilter::make('phone')->exact(),
            ...StringFilter::make('email')->exact(),
        ]);

        $this->defaultSort('id');
    }
}

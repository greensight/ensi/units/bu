<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\Store;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class StoresQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Store::query());

        $this->allowedIncludes([
            AllowedInclude::callback('workings', function ($query) {
                return $query->orderBy('id', 'desc');
            }),
            AllowedInclude::callback('contacts', function ($query) {
                return $query->orderBy('id', 'desc');
            }),
            AllowedInclude::callback('pickup_times', function ($query) {
                return $query->orderBy('id', 'desc');
            }, 'pickupTimes'),
            'contact',
        ]);

        $this->allowedSorts([
            'id',
            'seller_id',
            'xml_id',
            'active',
            'name',
            'timezone',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            ...NumericFilter::make('id')->exact(),
            ...NumericFilter::make('seller_id')->exact(),
            ...StringFilter::make('xml_id')->exact(),
            ...NumericFilter::make('active')->exact(),
            ...StringFilter::make('name')->exact()->contain(),
            ...StringFilter::make('timezone')->exact(),
            ExtraFilter::contain('address_string', 'address->address_string'),
            ...StringFilter::make('contact_name', 'contacts.name')->exact(),
            ...StringFilter::make('contact_phone', 'contacts.phone')->exact(),
            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);

        $this->defaultSort('id');
    }
}

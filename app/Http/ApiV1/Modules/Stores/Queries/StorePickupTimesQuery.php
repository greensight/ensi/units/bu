<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\StorePickupTime;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StorePickupTimesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(StorePickupTime::query());

        $this->allowedSorts([
            'id',
            'store_id',
            'day',
            'pickup_time_code',
            'pickup_time_start',
            'pickup_time_end',
            'delivery_service',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::callback('seller_id', function (Builder $query, $value) {
                $query->whereHas('store', fn (Builder $relationQuery) => $relationQuery->where('seller_id', $value));
            }),
            ...NumericFilter::make('id')->exact(),
            ...NumericFilter::make('store_id')->exact(),
            ...NumericFilter::make('day')->exact(),
            ...StringFilter::make('pickup_time_code')->exact(),
            ...StringFilter::make('pickup_time_start')->exact(),
            ...StringFilter::make('pickup_time_end')->exact(),
            ...NumericFilter::make('delivery_service')->exact(),
        ]);

        $this->defaultSort('id');
    }
}

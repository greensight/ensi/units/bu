<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateStoreRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer', Rule::exists(Seller::class, 'id')],
            'xml_id' => ['nullable', 'string', Rule::unique(Store::class)],
            'active' => ['required', 'boolean'],
            'name' => ['required', 'string'],
            ...self::addressField('address'),
            'timezone' => ['nullable', 'string'],
        ];
    }
}

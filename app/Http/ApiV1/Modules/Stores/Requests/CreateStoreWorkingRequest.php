<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateStoreWorkingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer', Rule::exists(Seller::class, 'id')],
            'store_id' => ['required', 'integer', Rule::exists(Store::class, 'id')],
            'active' => ['required', 'bool'],
            'day' => ['required', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
        ];
    }
}

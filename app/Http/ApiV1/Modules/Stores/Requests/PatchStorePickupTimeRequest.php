<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchStorePickupTimeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer', Rule::exists(Store::class, 'id')],
            'day' => ['sometimes', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['sometimes', 'string'],
            'pickup_time_end' => ['sometimes', 'string'],
            'delivery_service' => ['nullable', 'integer'],
        ];
    }
}

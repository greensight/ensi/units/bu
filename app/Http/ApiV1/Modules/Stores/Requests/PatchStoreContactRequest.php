<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchStoreContactRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer', Rule::exists(Store::class, 'id')],
            'name' => ['nullable', 'string'],
            'phone' => ['nullable', 'string'],
            'email' => ['nullable', 'string'],
        ];
    }
}

<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ExtendedPatchStoreWorkingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::nestedRules('fields', PatchStoreWorkingRequest::baseRules());
    }

    public function getFields(): array
    {
        return $this->input('fields', []);
    }
}

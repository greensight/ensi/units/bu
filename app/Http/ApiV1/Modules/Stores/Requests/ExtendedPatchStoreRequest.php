<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ExtendedPatchStoreRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::nestedRules('fields', PatchStoreRequest::baseRules($this->getRouteId()));
    }

    public function getFields(): array
    {
        return $this->input('fields', []);
    }
}

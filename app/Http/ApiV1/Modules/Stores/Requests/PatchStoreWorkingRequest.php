<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchStoreWorkingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            'store_id' => ['sometimes', 'integer', Rule::exists(Store::class, 'id')],
            'active' => ['sometimes', 'bool'],
            'day' => ['sometimes', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
        ];
    }
}

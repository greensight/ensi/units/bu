<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateStorePickupTimeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer', Rule::exists(Seller::class, 'id')],
            'store_id' => ['required', 'integer', Rule::exists(Store::class, 'id')],
            'day' => ['required', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['required', 'string'],
            'pickup_time_end' => ['required', 'string'],
            'delivery_service' => ['nullable', 'integer'],
        ];
    }
}

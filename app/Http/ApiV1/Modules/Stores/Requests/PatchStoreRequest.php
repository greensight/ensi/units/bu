<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchStoreRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules($this->getRouteId());
    }

    public static function baseRules(int $storeId): array
    {
        return [
            'seller_id' => ['nullable', 'integer', Rule::exists(Seller::class, 'id')],
            'xml_id' => ['nullable', 'string', Rule::unique(Store::class, 'xml_id')->ignore($storeId)],
            'active' => ['sometimes', 'boolean'],
            'name' => ['sometimes', 'string'],
            ...self::addressField('address'),
            'timezone' => ['nullable', 'string'],
        ];
    }
}

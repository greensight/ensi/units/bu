<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Domain\Stores\Models\StoreContact;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin StoreContact */
class StoreContactsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

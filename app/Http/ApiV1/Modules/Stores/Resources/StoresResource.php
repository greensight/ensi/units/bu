<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Domain\Stores\Models\Store;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Store */
class StoresResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'seller_id' => $this->seller_id,
            'xml_id' => $this->xml_id,
            'active' => $this->active,
            'name' => $this->name,
            'address' => $this->address,
            'timezone' => $this->timezone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'workings' => StoreWorkingsResource::collection($this->whenLoaded('workings')),
            'contacts' => StoreContactsResource::collection($this->whenLoaded('contacts')),
            'contact' => StoreContactsResource::make($this->whenLoaded('contact')),
            'pickup_times' => StorePickupTimesResource::collection($this->whenLoaded('pickupTimes')),
        ];
    }
}

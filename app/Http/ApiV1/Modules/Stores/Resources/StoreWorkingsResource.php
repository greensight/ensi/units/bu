<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Domain\Stores\Models\StoreWorking;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin StoreWorking */
class StoreWorkingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'active' => $this->active,
            'day' => $this->day,
            'working_start_time' => $this->working_start_time,
            'working_end_time' => $this->working_end_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

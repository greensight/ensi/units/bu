<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Domain\Stores\Models\StorePickupTime;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin StorePickupTime */
class StorePickupTimesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'day' => $this->day,
            'pickup_time_code' => $this->pickup_time_code,
            'pickup_time_start' => $this->pickup_time_start,
            'pickup_time_end' => $this->pickup_time_end,
            'delivery_service' => $this->delivery_service,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

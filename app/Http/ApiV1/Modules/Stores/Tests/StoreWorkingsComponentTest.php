<?php

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreWorking;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\CreateStoreWorkingRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\ExtendedPatchStoreWorkingRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\PatchStoreWorkingRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'stores');

test('POST /api/v1/stores/workings 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $store = Store::factory()->create();
    $request = CreateStoreWorkingRequestFactory::new()
        ->withStore($store->id)
        ->withSeller(null)
        ->make();

    $id = postJson('/api/v1/stores/workings', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(StoreWorking::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/workings verify seller_id 201', function (bool $isCorrectSeller) {
    $seller = Seller::factory()->create();
    $otherSeller = Seller::factory()->create();

    $store = Store::factory()->for($seller)->create();

    $request = CreateStoreWorkingRequestFactory::new()
        ->withStore($store->id)
        ->withSeller($isCorrectSeller ? $seller->id : $otherSeller->id)
        ->make();

    $response = postJson('/api/v1/stores/workings', $request);

    if ($isCorrectSeller) {
        $id = $response->assertStatus(201)->json('data.id');

        assertDatabaseHas(StoreWorking::class, ['id' => $id]);
    } else {
        $response->assertStatus(400);
    }
})->with([true, false]);

test('POST /api/v1/stores/workings 400', function () {
    $request = PatchStoreWorkingRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/stores/workings', $request)
        ->assertStatus(400);
});

test('GET /api/v1/stores/workings/{id} 200', function () {
    $model = StoreWorking::factory()->create();

    getJson("/api/v1/stores/workings/$model->id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id);
});

test('GET /api/v1/stores/workings/{id} 404', function () {
    getJson('/api/v1/stores/workings/1')
        ->assertStatus(404);
});

test('DELETE /api/v1/stores/workings/{id} 200', function () {
    $model = StoreWorking::factory()->create();

    deleteJson("/api/v1/stores/workings/$model->id")
        ->assertStatus(200);

    assertModelMissing($model);
});

test('DELETE /api/v1/stores/workings/{id} with filter 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StoreWorking::factory()->for($store)->create();

    deleteJson("/api/v1/stores/workings/{$model->id}", ["filter" => $filter])
        ->assertStatus($status);

    if ($status === 200) {
        assertModelMissing($model);
    } else {
        assertModelExists($model);
    }
})->with([
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/workings/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StoreWorking::factory()->create(['working_start_time' => '10:00']);
    $request = PatchStoreWorkingRequestFactory::new()->withStore($model->store_id)->make(['working_start_time' => '09:00']);

    patchJson("/api/v1/stores/workings/$model->id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id)
        ->assertJsonPath('data.working_start_time', $request['working_start_time']);

    assertDatabaseHas(StoreWorking::class, [
        'id' => $model->id,
        'working_start_time' => $request['working_start_time'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/workings/{id} 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StoreWorking::factory()->create();
    $request = PatchStoreWorkingRequestFactory::new()->withStore($model->store_id)->make(['store_id' => 'wrong_value']);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/stores/workings/$model->id", $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/workings/{id} 404', function () {
    patchJson('/api/v1/stores/workings/1')
        ->assertStatus(404);
});

test('POST /api/v1/stores/workings:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = StoreWorking::factory()->forEachSequence(
        ['working_start_time' => '09:00'],
        ['working_start_time' => '10:00'],
    )->create();

    $request = [
        'filter' => ['working_start_time' => '09:00'],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/workings:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $models->first()->id)
        ->assertJsonPath('data.0.working_start_time', $models->first()->working_start_time);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/workings:search 200 filter success', function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    $model = StoreWorking::factory()->create($value ? [$fieldKey => $value] : []);

    $request = [
        'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey})],
        'sort' => ['id'],
        'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ];
    postJson('/api/v1/stores/workings:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['store_id', fn () => Store::factory()->create()->id, null, null],
    ['active', true, null, null],
    ['working_start_time', '09:00', null, null],
    ['working_end_time', '21:00', null, null],
]);

test('POST /api/v1/stores/workings:search 200 filter seller_id success', function (bool $changeSellerId) {
    $seller = Seller::factory()->create();
    $store = Store::factory()->for($seller)->create();
    StoreWorking::factory()->for($store)->create();

    $request = [
        'filter' => ['seller_id' => $changeSellerId ? $seller->id + 1 : $seller->id],
    ];
    postJson('/api/v1/stores/workings:search', $request)
        ->assertStatus(200)
        ->assertJsonCount($changeSellerId ? 0 : 1, 'data');
})->with([true, false]);

test('POST /api/v1/stores/workings:search 200 sort success', function (string $sort) {
    StoreWorking::factory()->create();

    postJson('/api/v1/stores/workings:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'store_id',
    'active',
    'working_start_time',
    'working_end_time',
    'created_at',
    'updated_at',
]);

test('PATCH /api/v1/stores/workings/{id}:extended 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StoreWorking::factory()->for($store)->create();

    $request = ExtendedPatchStoreWorkingRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter($filter)
        ->make();

    patchJson("/api/v1/stores/workings/$model->id:extended", $request)
        ->assertStatus($status);

    if ($status === 200) {
        assertDatabaseHas(StoreWorking::class, ['id' => $model->id, 'day' => data_get($request, 'fields.day')]);
    }
})->with([
    [1000, [], 200],
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/workings/{id}:extended 404', function () {
    $request = ExtendedPatchStoreWorkingRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter([])
        ->make();

    patchJson('/api/v1/stores/workings/404:extended', $request)
        ->assertStatus(404);
});

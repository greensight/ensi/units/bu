<?php

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreContact;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\CreateStoreContactRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\ExtendedPatchStoreContactRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\PatchStoreContactRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'stores');

test('POST /api/v1/stores/contacts 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $store = Store::factory()->create();
    $request = CreateStoreContactRequestFactory::new()
        ->withStore($store->id)
        ->withSeller(null)
        ->make();

    $id = postJson('/api/v1/stores/contacts', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(StoreContact::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/contacts verify seller_id 201', function (bool $isCorrectSeller) {
    $seller = Seller::factory()->create();
    $otherSeller = Seller::factory()->create();

    $store = Store::factory()->for($seller)->create();

    $request = CreateStoreContactRequestFactory::new()
        ->withStore($store->id)
        ->withSeller($isCorrectSeller ? $seller->id : $otherSeller->id)
        ->make();

    $response = postJson('/api/v1/stores/contacts', $request);

    if ($isCorrectSeller) {
        $id = $response->assertStatus(201)->json('data.id');

        assertDatabaseHas(StoreContact::class, ['id' => $id]);
    } else {
        $response->assertStatus(400);
    }
})->with([true, false]);

test('POST /api/v1/stores/contacts 400', function () {
    $request = CreateStoreContactRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/stores/contacts', $request)
        ->assertStatus(400);
});

test('GET /api/v1/stores/contacts/{id} 200', function () {
    $model = StoreContact::factory()->create();

    getJson("/api/v1/stores/contacts/$model->id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id);
});

test('GET /api/v1/stores/contacts/{id} 404', function () {
    getJson('/api/v1/stores/contacts/1')
        ->assertStatus(404);
});

test('DELETE /api/v1/stores/contacts/{id} 200', function () {
    $model = StoreContact::factory()->create();

    deleteJson("/api/v1/stores/contacts/$model->id")
        ->assertStatus(200);

    assertModelMissing($model);
});

test('DELETE /api/v1/stores/contacts/{id} with filter 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StoreContact::factory()->for($store)->create();

    deleteJson("/api/v1/stores/contacts/{$model->id}", ["filter" => $filter])
        ->assertStatus($status);

    if ($status === 200) {
        assertModelMissing($model);
    } else {
        assertModelExists($model);
    }
})->with([
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/contacts/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StoreContact::factory()->create(['name' => 'Some old name']);
    $request = PatchStoreContactRequestFactory::new()->withStore($model->store_id)->make(['name' => 'New Name']);

    patchJson("/api/v1/stores/contacts/$model->id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id)
        ->assertJsonPath('data.name', $request['name']);

    assertDatabaseHas(StoreContact::class, [
        'id' => $model->id,
        'name' => $request['name'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/contacts/{id} 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StoreContact::factory()->create();
    $request = PatchStoreContactRequestFactory::new()->withStore($model->store_id)->make(['store_id' => 'wrong_value']);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/stores/contacts/$model->id", $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/contacts/{id} 404', function () {
    patchJson('/api/v1/stores/contacts/1')
        ->assertStatus(404);
});

test('POST /api/v1/stores/contacts:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = StoreContact::factory()->forEachSequence(
        ['name' => 'Foo'],
        ['name' => 'Bar'],
    )->create();

    $request = [
        'filter' => ['name' => 'Foo'],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/contacts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $models->first()->id)
        ->assertJsonPath('data.0.name', $models->first()->name);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/contacts:search 200 filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    $model = StoreContact::factory()->create($value ? [$fieldKey => $value] : []);

    $request = [
        'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey})],
        'sort' => ['id'],
        'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ];
    postJson('/api/v1/stores/contacts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['store_id', fn () => Store::factory()->create()->id, null, null],
    ['name', 'Foo', null, null],
    ['phone', '+79876543210', null, null],
    ['email', 'ivanov@example.com', null, null],
]);

test('POST /api/v1/stores/contacts:search 200 filter seller_id success', function (bool $changeSellerId) {
    $seller = Seller::factory()->create();
    $store = Store::factory()->for($seller)->create();
    StoreContact::factory()->for($store)->create();

    $request = [
        'filter' => ['seller_id' => $changeSellerId ? $seller->id + 1 : $seller->id],
    ];
    postJson('/api/v1/stores/contacts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount($changeSellerId ? 0 : 1, 'data');
})->with([true, false]);

test('POST /api/v1/stores/contacts:search 200 sort success', function (string $sort) {
    StoreContact::factory()->create();

    postJson('/api/v1/stores/contacts:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'store_id',
    'name',
    'phone',
    'email',
    'created_at',
    'updated_at',
]);

test('PATCH /api/v1/stores/contacts/{id}:extended 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StoreContact::factory()->for($store)->create();

    $request = ExtendedPatchStoreContactRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter($filter)
        ->make();

    patchJson("/api/v1/stores/contacts/$model->id:extended", $request)
        ->assertStatus($status);

    if ($status === 200) {
        assertDatabaseHas(StoreContact::class, ['id' => $model->id, 'name' => data_get($request, 'fields.name')]);
    }
})->with([
    [1000, [], 200],
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/contacts/{id}:extended 404', function () {
    $request = ExtendedPatchStoreContactRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter([])
        ->make();

    patchJson('/api/v1/stores/contacts/404:extended', $request)
        ->assertStatus(404);
});

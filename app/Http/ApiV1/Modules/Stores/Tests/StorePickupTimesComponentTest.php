<?php

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StorePickupTime;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\CreateStorePickupTimeRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\ExtendedPatchStorePickupTimeRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\PatchStorePickupTimeRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'stores');

test('POST /api/v1/stores/pickup-times 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $store = Store::factory()->create();
    $request = CreateStorePickupTimeRequestFactory::new()
        ->withStore($store->id)
        ->withSeller(null)
        ->make();

    $id = postJson('/api/v1/stores/pickup-times', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(StorePickupTime::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/pickup-times verify seller_id 201', function (bool $isCorrectSeller) {
    $seller = Seller::factory()->create();
    $otherSeller = Seller::factory()->create();

    $store = Store::factory()->for($seller)->create();

    $request = CreateStorePickupTimeRequestFactory::new()
        ->withStore($store->id)
        ->withSeller($isCorrectSeller ? $seller->id : $otherSeller->id)
        ->make();

    $response = postJson('/api/v1/stores/pickup-times', $request);

    if ($isCorrectSeller) {
        $id = $response->assertStatus(201)->json('data.id');

        assertDatabaseHas(StorePickupTime::class, ['id' => $id]);
    } else {
        $response->assertStatus(400);
    }
})->with([true, false]);

test('POST /api/v1/stores/pickup-times 400', function () {
    $request = PatchStorePickupTimeRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/stores/pickup-times', $request)
        ->assertStatus(400);
});

test('GET /api/v1/stores/pickup-times/{id} 200', function () {
    $model = StorePickupTime::factory()->create();

    getJson("/api/v1/stores/pickup-times/$model->id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id);
});

test('GET /api/v1/stores/pickup-times/{id} 404', function () {
    getJson('/api/v1/stores/pickup-times/1')
        ->assertStatus(404);
});

test('DELETE /api/v1/stores/pickup-times/{id} 200', function () {
    $model = StorePickupTime::factory()->create();

    deleteJson("/api/v1/stores/pickup-times/$model->id")
        ->assertStatus(200);

    assertModelMissing($model);
});

test('DELETE /api/v1/stores/pickup-times/{id} with filter 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StorePickupTime::factory()->for($store)->create();

    deleteJson("/api/v1/stores/pickup-times/{$model->id}", ["filter" => $filter])
        ->assertStatus($status);

    if ($status === 200) {
        assertModelMissing($model);
    } else {
        assertModelExists($model);
    }
})->with([
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/pickup-times/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StorePickupTime::factory()->create(['pickup_time_start' => '10:00:00']);
    $request = PatchStorePickupTimeRequestFactory::new()->withStore($model->store_id)->make(['pickup_time_start' => '09:00:00']);

    patchJson("/api/v1/stores/pickup-times/$model->id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id)
        ->assertJsonPath('data.pickup_time_start', $request['pickup_time_start']);

    assertDatabaseHas(StorePickupTime::class, [
        'id' => $model->id,
        'pickup_time_start' => $request['pickup_time_start'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/pickup-times/{id} 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = StorePickupTime::factory()->create();
    $request = PatchStorePickupTimeRequestFactory::new()->withStore($model->store_id)->make(['store_id' => 'wrong_value']);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/stores/pickup-times/$model->id", $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/pickup-times/{id} 404', function () {
    patchJson('/api/v1/stores/pickup-times/1')
        ->assertStatus(404);
});

test('POST /api/v1/stores/pickup-times:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = StorePickupTime::factory()->forEachSequence(
        ['day' => 1],
        ['day' => 2],
    )->create();

    $request = [
        'filter' => ['day' => 1],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/pickup-times:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $models->first()->id)
        ->assertJsonPath('data.0.day', $models->first()->day);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/pickup-times:search 200 filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
) {
    $model = StorePickupTime::factory()->create($value ? [$fieldKey => $value] : []);

    $request = [
        'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey})],
        'sort' => ['id'],
        'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ];
    postJson('/api/v1/stores/pickup-times:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['store_id', fn () => Store::factory()->create()->id, null, null],
    ['day', 1, null, null],
    ['pickup_time_code', '15-23', null, null],
    ['pickup_time_start', '09:00:00', null, null],
    ['pickup_time_end', '22:00:00', null, null],
    ['delivery_service', 1, null, null],
]);

test('POST /api/v1/stores/pickup-times:search 200 filter seller_id success', function (bool $changeSellerId) {
    $seller = Seller::factory()->create();
    $store = Store::factory()->for($seller)->create();
    StorePickupTime::factory()->for($store)->create();

    $request = [
        'filter' => ['seller_id' => $changeSellerId ? $seller->id + 1 : $seller->id],
    ];
    postJson('/api/v1/stores/pickup-times:search', $request)
        ->assertStatus(200)
        ->assertJsonCount($changeSellerId ? 0 : 1, 'data');
})->with([true, false]);

test('POST /api/v1/stores/pickup-times:search 200 sort success', function (string $sort) {
    StorePickupTime::factory()->create();

    postJson('/api/v1/stores/pickup-times:search', ['sort' => [$sort]])->assertStatus(200);
})->with([
    'id',
    'store_id',
    'day',
    'pickup_time_code',
    'pickup_time_start',
    'pickup_time_end',
    'delivery_service',
    'created_at',
    'updated_at',
]);

test('PATCH /api/v1/stores/pickup-times/{id}:extended 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);
    $store = Store::factory()->for($seller)->create();
    $model = StorePickupTime::factory()->for($store)->create();

    $request = ExtendedPatchStorePickupTimeRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter($filter)
        ->make();

    patchJson("/api/v1/stores/pickup-times/$model->id:extended", $request)
        ->assertStatus($status);

    if ($status === 200) {
        assertDatabaseHas(StorePickupTime::class, ['id' => $model->id, 'day' => data_get($request, 'fields.day')]);
    }
})->with([
    [1000, [], 200],
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/pickup-times/{id}:extended 404', function () {
    $request = ExtendedPatchStorePickupTimeRequestFactory::new()
        ->withoutFields(['store_id'])
        ->withFilter([])
        ->make();

    patchJson('/api/v1/stores/pickup-times/404:extended', $request)
        ->assertStatus(404);
});

<?php

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreContact;
use App\Domain\Stores\Models\StorePickupTime;
use App\Domain\Stores\Models\StoreWorking;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\ExtendedPatchStoreRequestFactory;
use App\Http\ApiV1\Modules\Stores\Tests\Factories\StoreRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

use PHPUnit\Framework\Assert;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'stores');

test('POST /api/v1/stores/stores 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $seller = Seller::factory()->create();
    $request = StoreRequestFactory::new()->withSeller($seller->id)->make();

    $id = postJson('/api/v1/stores/stores', $request)
        ->assertStatus(201)
        ->json('data.id');

    assertDatabaseHas(Store::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/stores/stores/{id} 200', function () {
    $model = Store::factory()->create();

    getJson("/api/v1/stores/stores/$model->id")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id);
});

test('GET /api/v1/stores/stores/{id} 404', function () {
    getJson('/api/v1/stores/stores/1')
        ->assertStatus(404);
});

test('PATCH /api/v1/stores/stores/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Store::factory()->create(['name' => 'Some old name']);
    $request = StoreRequestFactory::new()->withSeller($model->seller_id)->make(['name' => 'New Name']);

    patchJson("/api/v1/stores/stores/$model->id", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id)
        ->assertJsonPath('data.name', $request['name']);

    assertDatabaseHas(Store::class, [
        'id' => $model->id,
        'name' => $request['name'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/stores/{id} 400', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Store::factory()->create();
    $request = StoreRequestFactory::new()->withSeller($model->seller_id)->make(['seller_id' => 'wrong_value']);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/stores/stores/$model->id", $request)
        ->assertStatus(400);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/stores/stores/{id} 404', function () {
    patchJson('/api/v1/stores/stores/1')
        ->assertStatus(404);
});

test('POST /api/v1/stores/stores:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Store::factory()->forEachSequence(
        ['name' => 'Foo'],
        ['name' => 'Bar'],
    )->create();

    $request = [
        'filter' => ['name' => 'Foo'],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/stores:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $models->first()->id)
        ->assertJsonPath('data.0.name', $models->first()->name);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search 200 filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    if (in_array($fieldKey, ['contacts.name', 'contacts.phone'])) {
        $contactField = match ($fieldKey) {
            'contacts.name' => 'name',
            'contacts.phone' => 'phone',
        };
        $model = Store::factory()
            ->has(StoreContact::factory()->state([$contactField => $value]), 'contacts')
            ->create();
    } else {
        $model = Store::factory()->create($value ? [$fieldKey => $value] : []);
    }

    $request = [
        'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey})],
        'sort' => ['id'],
        'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ];
    postJson('/api/v1/stores/stores:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['seller_id', fn () => Seller::factory()->create()->id, null, null],
    ['xml_id', 'centralnyi_sklad', null, null],
    ['active', true, null, null],
    ['name', 'Центральный склад', null, null],
    ['timezone', 'Europe/Moscow', null, null],
    [
        'address',
        fn () => AddressData::factory()->make(['address_string' => 'Москва, ул. Центральная, 1'])->toArray(),
        'address_string',
        'Москва, ул. Центральная, 1',
    ],
    ['contacts.name', 'Иванов Иван Иванович', 'contact_name', 'Иванов Иван Иванович'],
    ['contacts.phone', '+71112223344', 'contact_phone', '+71112223344'],
    ['created_at', '2023-05-15T10:43:06.000000Z', 'created_at_gte', '2023-05-14T10:43:06.000000Z'],
    ['created_at', '2023-05-15T10:43:06.000000Z', 'created_at_lte', '2023-05-16T10:43:06.000000Z'],
    ['updated_at', '2023-05-15T10:43:06.000000Z', 'updated_at_gte', '2023-05-14T10:43:06.000000Z'],
    ['updated_at', '2023-05-15T10:43:06.000000Z', 'updated_at_lte', '2023-05-16T10:43:06.000000Z'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search 200 sort success', function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Store::factory()->create();

    $request = ['sort' => [$sort]];
    postJson('/api/v1/stores/stores:search', $request)
        ->assertStatus(200);
})->with([
    'id',
    'seller_id',
    'xml_id',
    'active',
    'name',
    'timezone',
    'created_at',
    'updated_at',
], FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search 200 include success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Store::factory()
        ->has(StoreWorking::factory()->count(7), 'workings')
        ->has(StorePickupTime::factory()->count(7), 'pickupTimes')
        ->has(StoreContact::factory()->count(4), 'contacts')
        ->create();

    $expectedWorkingsIds = $model->workings()->get()->sortByDesc(['id'])->pluck('id')->toArray();
    $expectedPickupTimeIds = $model->pickupTimes()->get()->sortByDesc(['id'])->pluck('id')->toArray();
    $expectedContactsIds = $model->contacts()->get()->sortByDesc(['id'])->pluck('id')->toArray();

    $request = [
        'include' => ['workings', 'contacts', 'pickup_times', 'contact'],
    ];

    $response = postJson('/api/v1/stores/stores:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id)
        ->assertJsonCount(7, 'data.0.workings')
        ->assertJsonCount(7, 'data.0.pickup_times')
        ->assertJsonCount(4, 'data.0.contacts')
        ->assertJsonPath('data.0.contact', fn (?array $contact) => $contact !== null);


    $responseWorkingsIds = Arr::pluck($response->json('data.0.workings'), 'id');
    $responsePickupTimeIds = Arr::pluck($response->json('data.0.pickup_times'), 'id');
    $responseContactsIds = Arr::pluck($response->json('data.0.contacts'), 'id');

    Assert::assertEquals($expectedWorkingsIds, $responseWorkingsIds);
    Assert::assertEquals($expectedPickupTimeIds, $responsePickupTimeIds);
    Assert::assertEquals($expectedContactsIds, $responseContactsIds);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search-one 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Store::factory()->forEachSequence(
        ['name' => 'Foo'],
        ['name' => 'Bar'],
    )->create();

    $request = [
        'filter' => ['name' => 'Foo'],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/stores:search-one', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $models->first()->id)
        ->assertJsonPath('data.name', $models->first()->name);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search-one 200 filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    if (in_array($fieldKey, ['contacts.name', 'contacts.phone'])) {
        $contactField = match ($fieldKey) {
            'contacts.name' => 'name',
            'contacts.phone' => 'phone',
        };
        $model = Store::factory()
            ->has(StoreContact::factory()->state([$contactField => $value]), 'contacts')
            ->create();
    } else {
        $model = Store::factory()->create($value ? [$fieldKey => $value] : []);
    }

    $request = [
        'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey})],
        'sort' => ['id'],
        'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
    ];
    postJson('/api/v1/stores/stores:search-one', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id);
})->with([
    ['seller_id', fn () => Seller::factory()->create()->id, null, null],
    ['xml_id', 'centralnyi_sklad', null, null],
    ['active', true, null, null],
    ['name', 'Центральный склад', null, null],
    ['timezone', 'Europe/Moscow', null, null],
    [
        'address',
        fn () => AddressData::factory()->make(['address_string' => 'Москва, ул. Центральная, 1'])->toArray(),
        'address_string',
        'Москва, ул. Центральная, 1',
    ],
    ['contacts.name', 'Иванов Иван Иванович', 'contact_name', 'Иванов Иван Иванович'],
    ['contacts.phone', '+71112223344', 'contact_phone', '+71112223344'],
    ['created_at', '2023-05-15T10:43:06.000000Z', 'created_at_gte', '2023-05-14T10:43:06.000000Z'],
    ['created_at', '2023-05-15T10:43:06.000000Z', 'created_at_lte', '2023-05-16T10:43:06.000000Z'],
    ['updated_at', '2023-05-15T10:43:06.000000Z', 'updated_at_gte', '2023-05-14T10:43:06.000000Z'],
    ['updated_at', '2023-05-15T10:43:06.000000Z', 'updated_at_lte', '2023-05-16T10:43:06.000000Z'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search-one 200 sort success', function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    Store::factory()->create();

    $request = ['sort' => [$sort]];
    postJson('/api/v1/stores/stores:search-one', $request)
        ->assertStatus(200);
})->with([
    'id',
    'seller_id',
    'xml_id',
    'active',
    'name',
    'timezone',
    'created_at',
    'updated_at',
], FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search-one 200 include success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $model = Store::factory()
        ->has(StoreWorking::factory()->count(7), 'workings')
        ->has(StorePickupTime::factory()->count(7), 'pickupTimes')
        ->has(StoreContact::factory()->count(4), 'contacts')
        ->create();

    $request = [
        'include' => ['workings', 'contacts', 'pickup_times', 'contact'],
    ];
    postJson('/api/v1/stores/stores:search-one', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $model->id)
        ->assertJsonCount(7, 'data.workings')
        ->assertJsonCount(7, 'data.pickup_times')
        ->assertJsonCount(4, 'data.contacts')
        ->assertJsonPath('data.contact', fn (?array $contact) => $contact !== null);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/stores/stores:search-one 404', function () {
    Store::factory()->forEachSequence(
        ['name' => 'Foo'],
        ['name' => 'Bar'],
    )->create();

    $request = [
        'filter' => ['name' => 'SomeName'],
        'sort' => ['id'],
    ];
    postJson('/api/v1/stores/stores:search-one', $request)
        ->assertStatus(404);
});

test('PATCH /api/v1/stores/stores/{id}:extended 200', function (int $sellerId, array $filter, int $status) {
    $seller = Seller::factory()->create(['id' => $sellerId]);

    $model = Store::factory()->for($seller)->create();

    $request = ExtendedPatchStoreRequestFactory::new()
        ->withoutFields(['seller_id'])
        ->withFilter($filter)
        ->make();

    patchJson("/api/v1/stores/stores/{$model->id}:extended", $request)
        ->assertStatus($status);

    if ($status === 200) {
        assertDatabaseHas(Store::class, ['id' => $model->id, 'name' => data_get($request, 'fields.name')]);
    }
})->with([
    [1000, [], 200],
    [1000, ['seller_id' => 1000], 200],
    [1000, ['seller_id' => 1001], 404],
]);

test('PATCH /api/v1/stores/stores/{id}:extended 404', function () {
    $request = ExtendedPatchStoreRequestFactory::new()
        ->withoutFields(['seller_id'])
        ->withFilter([])
        ->make();

    patchJson('/api/v1/stores/stores/404:extended', $request)
        ->assertStatus(404);
});

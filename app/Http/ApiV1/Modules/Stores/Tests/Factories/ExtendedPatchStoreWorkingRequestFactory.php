<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\ExtendedPatchRequestFactory;

class ExtendedPatchStoreWorkingRequestFactory extends ExtendedPatchRequestFactory
{
    protected function getFields(): array
    {
        return PatchStoreWorkingRequestFactory::new()->make();
    }

    protected function getFilter(): array
    {
        return $this->sellerFilter();
    }
}

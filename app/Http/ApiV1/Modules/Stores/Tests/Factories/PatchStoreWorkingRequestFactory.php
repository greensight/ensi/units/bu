<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchStoreWorkingRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->nullable()->time('H:i'),
            'working_end_time' => $this->faker->nullable()->time('H:i'),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withStore(?int $storeId): static
    {
        return $this->state(['store_id' => $storeId]);
    }
}

<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\ExtendedPatchRequestFactory;

class ExtendedPatchStoreContactRequestFactory extends ExtendedPatchRequestFactory
{
    protected function getFields(): array
    {
        return PatchStoreContactRequestFactory::new()->make();
    }

    protected function getFilter(): array
    {
        return $this->sellerFilter();
    }
}

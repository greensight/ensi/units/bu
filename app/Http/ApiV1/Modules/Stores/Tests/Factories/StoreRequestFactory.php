<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use App\Domain\Common\Data\AddressData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seller_id' => $this->faker->nullable()->modelId(),
            'xml_id' => $this->faker->nullable()->exactly($this->faker->unique()->word()),
            'active' => $this->faker->boolean(),
            'name' => $this->faker->name(),
            'address' => $this->faker->nullable()->passthrough(AddressData::factory()->make()->toArray()),
            'timezone' => $this->faker->timezone(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withSeller(?int $sellerId): static
    {
        return $this->state(['seller_id' => $sellerId]);
    }
}

<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\ExtendedPatchRequestFactory;

class ExtendedPatchStorePickupTimeRequestFactory extends ExtendedPatchRequestFactory
{
    protected function getFields(): array
    {
        return PatchStorePickupTimeRequestFactory::new()->make();
    }

    protected function getFilter(): array
    {
        return $this->sellerFilter();
    }
}

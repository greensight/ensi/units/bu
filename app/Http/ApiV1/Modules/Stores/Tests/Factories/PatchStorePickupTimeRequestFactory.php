<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchStorePickupTimeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => $this->faker->nullable()->passthrough(
                "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}"
            ),
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'delivery_service' => null,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withStore(?int $storeId): static
    {
        return $this->state(['store_id' => $storeId]);
    }
}

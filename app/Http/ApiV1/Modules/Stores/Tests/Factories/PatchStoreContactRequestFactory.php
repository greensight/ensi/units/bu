<?php

namespace App\Http\ApiV1\Modules\Stores\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchStoreContactRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->word(),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'email' => $this->faker->nullable()->email(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withStore(?int $storeId): static
    {
        return $this->state(['store_id' => $storeId]);
    }
}

<?php

namespace App\Http\ApiV1\Modules\Sellers\Controllers;

use App\Domain\Sellers\Actions\CreateSellerAction;
use App\Domain\Sellers\Actions\CreateWithOperatorSellerAction;
use App\Domain\Sellers\Actions\DeleteSellerAction;
use App\Domain\Sellers\Actions\PatchSellerAction;
use App\Domain\Sellers\Actions\RegisterSellerAction;
use App\Http\ApiV1\Modules\Sellers\Queries\SellersQuery;
use App\Http\ApiV1\Modules\Sellers\Requests\CreateSellerRequest;
use App\Http\ApiV1\Modules\Sellers\Requests\CreateSellerWithOperatorRequest;
use App\Http\ApiV1\Modules\Sellers\Requests\PatchSellerRequest;
use App\Http\ApiV1\Modules\Sellers\Requests\RegisterSellerWithOperatorRequest;
use App\Http\ApiV1\Modules\Sellers\Resources\SellersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class SellersController
{
    public function create(CreateSellerRequest $request, CreateSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchSellerRequest $request, PatchSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteSellerAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, SellersQuery $query): Responsable
    {
        return new SellersResource($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, SellersQuery $query): Responsable
    {
        return SellersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function createWithOperator(CreateSellerWithOperatorRequest $request, CreateWithOperatorSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function register(RegisterSellerWithOperatorRequest $request, RegisterSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($request->validated()));
    }
}

<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateSellerWithOperatorRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            ...BaseFormRequest::nestedRules('seller', CreateSellerRequest::baseRules()),

            // Информация оператора
            'operator' => ['required', 'array'],
            'operator.first_name' => ['nullable', 'string'],
            'operator.last_name' => ['nullable', 'string'],
            'operator.middle_name' => ['nullable', 'string'],
            'operator.email' => ['nullable', 'email'],
            'operator.phone' => ['nullable', 'regex:/^\+7\d{10}$/'],
            'operator.password' => ['nullable', 'string', 'min:8'],
        ];
    }
}

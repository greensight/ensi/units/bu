<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\Modules\Sellers\Rules\InnRule;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class PatchSellerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'manager_id' => ['nullable', 'integer'],
            'legal_name' => ['sometimes', 'string', Rule::unique(Seller::class, 'legal_name')->ignore($this->getRouteId())],
            ...self::addressField('legal_address'),
            ...self::addressField('fact_address'),
            'inn' => ['nullable', 'string', new InnRule()],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            ...self::addressField('bank_address'),
            'bank_bik' => ['nullable', 'string'],
            'status' => ['sometimes', 'integer', new Enum(SellerStatusEnum::class)],
            'site' => ['nullable', 'string'],
            'info' => ['nullable', 'string'],
        ];
    }
}

<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\Modules\Sellers\Rules\InnRule;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateSellerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            'manager_id' => ['nullable', 'integer'],
            'legal_name' => ['required', 'string', Rule::unique(Seller::class, 'legal_name')],
            ...self::addressField('legal_address'),
            ...self::addressField('fact_address'),
            'inn' => ['nullable', 'string', new InnRule()],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            ...self::addressField('bank_address'),
            'bank_bik' => ['nullable', 'string'],
            'site' => ['nullable', 'string'],
            'info' => ['nullable', 'string'],
        ];
    }
}

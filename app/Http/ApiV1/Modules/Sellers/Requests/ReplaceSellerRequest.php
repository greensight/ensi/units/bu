<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class ReplaceSellerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'legal_name' => ['required', 'string'],
            'external_id' => ['required', 'string'],
            'legal_address' => ['nullable', 'string'],
            'fact_address' => ['nullable', 'string'],
            'inn' => ['nullable', 'string'],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            'bank_address' => ['nullable', 'string'],
            'bank_bik' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'status' => ['nullable', 'integer', new Enum(SellerStatusEnum::class)],
            'manager_id' => ['nullable', 'integer'],
            'storage_address' => ['nullable', 'string'],
            'site' => ['nullable', 'string'],
            'can_integration' => ['nullable', 'boolean'],
            'sale_info' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
        ];
    }
}

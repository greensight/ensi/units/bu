<?php

namespace App\Http\ApiV1\Modules\Sellers\Resources;

use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\Modules\SellerUsers\Resources\OperatorsResource;
use App\Http\ApiV1\Modules\Stores\Resources\StoresResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Seller */
class SellersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'manager_id' => $this->manager_id,
            'legal_name' => $this->legal_name,
            'legal_address' => $this->legal_address,
            'fact_address' => $this->fact_address,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'payment_account' => $this->payment_account,
            'correspondent_account' => $this->correspondent_account,
            'bank' => $this->bank,
            'bank_address' => $this->bank_address,
            'bank_bik' => $this->bank_bik,
            'status' => $this->status,
            'status_at' => $this->status_at,
            'site' => $this->site,
            'info' => $this->info,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'operators' => OperatorsResource::collection($this->whenLoaded('operators')),
            'stores' => StoresResource::collection($this->whenLoaded('stores')),
        ];
    }
}

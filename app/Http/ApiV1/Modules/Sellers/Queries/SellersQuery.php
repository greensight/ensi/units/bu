<?php

namespace App\Http\ApiV1\Modules\Sellers\Queries;

use App\Domain\Sellers\Models\Seller;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SellersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Seller::query());

        $this->allowedIncludes(['operators', 'stores']);

        $this->allowedSorts([
            'id',
            'manager_id',
            'legal_name',
            'status',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('manager_id'),
            AllowedFilter::exact('status'),
            AllowedFilter::partial('legal_address_like', 'legal_address->address_string'),
            ...StringFilter::make('legal_name')->contain(),
            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);

        $this->defaultSort('id');
    }
}

<?php

namespace App\Http\ApiV1\Modules\Sellers\Tests\Factories;

use App\Domain\Common\Data\AddressData;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SellerRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'manager_id' => $this->faker->nullable()->modelId(),
            'legal_name' => $this->faker->unique()->company(),
            'legal_address' => $this->faker->nullable()->exactly(AddressData::factory()->makeArray()),
            'fact_address' => $this->faker->nullable()->exactly(AddressData::factory()->makeArray()),
            'inn' => $this->faker->exactly($this->callback(fn () => $this->inn())),
            'kpp' => $this->faker->nullable()->numerify('#########'),
            'payment_account' => $this->faker->nullable()->numerify('####################'),
            'correspondent_account' => $this->faker->nullable()->numerify('####################'),
            'bank' => $this->faker->nullable()->company(),
            'bank_address' => $this->faker->nullable()->exactly(AddressData::factory()->makeArray()),
            'bank_bik' => $this->faker->nullable()->numerify('#########'),
            'status' => $this->faker->randomEnum(SellerStatusEnum::cases()),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->nullable()->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function callback(callable $callback): mixed
    {
        return $callback();
    }

    protected function inn(): string
    {
        $coefficients = [2, 4, 10, 3, 5, 9, 4, 6, 8, 0];
        $numbers = $this->faker->randomList(fn () => $this->faker->numberBetween(1, 9), 9, 9);

        $value = 0;
        foreach ($numbers as $key => $number) {
            $value += $number * $coefficients[$key];
        }
        $value = $value % 11;
        $numbers[] = $value == 10 ? 0 : $value;

        return implode($numbers);
    }

    public function withLegalAddress(AddressData $legalAddress): static
    {
        return $this->state(['legal_address' => $legalAddress->toArray()]);
    }
}

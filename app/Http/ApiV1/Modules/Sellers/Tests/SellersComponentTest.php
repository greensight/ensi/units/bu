<?php

use App\Domain\Common\Data\AddressData;
use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\Modules\Sellers\Tests\Factories\SellerRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');


test('POST /api/v1/sellers/sellers 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = SellerRequestFactory::new()->make();

    $id = postJson('/api/v1/sellers/sellers', $request)
        ->assertCreated()
        ->json('data.id');

    assertDatabaseHas(Seller::class, ['id' => $id]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers check inn', function (string $inn, int $status, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = SellerRequestFactory::new()->make(['inn' => $inn]);

    postJson('/api/v1/sellers/sellers', $request)
        ->assertStatus($status);

})->with([
    'valid inn' => ['7743013901', 201],
    'not valid inn' => ['7743013902', 400],
], FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers check legal_name', function (string $legalSuffix, int $status) {
    /** @var Seller $seller */
    $seller = Seller::factory()->create();
    $request = SellerRequestFactory::new()->make(['legal_name' => $seller->legal_name . $legalSuffix]);

    postJson('/api/v1/sellers/sellers', $request)
        ->assertStatus($status);

})->with([
    'valid' => ['1', 201],
    'not valid' => ['', 400],
]);

test('POST /api/v1/sellers/sellers check legal_address', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $addressData = AddressData::factory()->make();
    $request = SellerRequestFactory::new()->withLegalAddress($addressData)->make();

    $id = postJson('/api/v1/sellers/sellers', $request)
        ->assertCreated()
        ->json('data.id');

    /** @var Seller $seller */
    $seller = Seller::query()->findOrFail($id);

    assertEquals($addressData, $seller->legal_address);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers 400', function () {
    $request = SellerRequestFactory::new()->make(['legal_name' => null]);

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/sellers/sellers', $request)
        ->assertStatus(400);
});

test('GET /api/v1/sellers/sellers/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Seller $seller */
    $seller = Seller::factory()->create();

    getJson("/api/v1/sellers/sellers/$seller->id")
        ->assertOk()
        ->assertJsonPath('data.id', $seller->id);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/sellers/sellers/{id} 404', function () {
    getJson('/api/v1/sellers/sellers/1')
        ->assertNotFound();
});

test('PATCH /api/v1/sellers/sellers/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Seller $seller */
    $seller = Seller::factory()->create();
    $request = SellerRequestFactory::new()->make(['legal_name' => "new_$seller->legal_name"]);

    patchJson("/api/v1/sellers/sellers/$seller->id", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $seller->id)
        ->assertJsonPath('data.legal_name', $request['legal_name']);

    assertDatabaseHas(Seller::class, [
        'id' => $seller->id,
        'legal_name' => $request['legal_name'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/sellers/sellers/{id} check legal_name', function (string $legal, string $legalOther, string $legalRequest, int $status) {
    /** @var Seller $seller */
    $seller = Seller::factory()->create(['legal_name' => $legal]);
    Seller::factory()->create(['legal_name' => $legalOther]);

    $request = SellerRequestFactory::new()->make(['legal_name' => $legalRequest]);

    patchJson("/api/v1/sellers/sellers/{$seller->id}", $request)
        ->assertStatus($status);

})->with([
    'current' => ['legal', 'other', 'legal', 200],
    'duplicate' => ['legal', 'other', 'other', 400],
    'unique' => ['legal', 'other', 'new', 200],
]);

test('PATCH /api/v1/sellers/sellers/{id} 404', function () {
    $request = SellerRequestFactory::new()->make();

    patchJson('/api/v1/sellers/sellers/404', $request)
        ->assertNotFound();
});

test('DELETE /api/v1/sellers/sellers/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Seller $seller */
    $seller = Seller::factory()->create();

    deleteJson("/api/v1/sellers/sellers/$seller->id")
        ->assertOk();

    assertModelMissing($seller);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Collection<int, Seller> $sellers */
    $sellers = Seller::factory()->forEachSequence(
        ['status' => SellerStatusEnum::ACTIVE->value],
        ['status' => SellerStatusEnum::NOT_ACTIVE->value],
    )->create();

    $request = [
        'filter' => ['status' => SellerStatusEnum::ACTIVE->value],
        'sort' => ['id'],
    ];
    postJson('/api/v1/sellers/sellers:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $sellers->first()->id)
        ->assertJsonPath('data.0.status', $sellers->first()->status->value);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers:search 200 filter success', function (
    string $fieldKey,
    $value,
    ?string $filterKey,
    $filterValue,
    ?bool $always,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var Seller $seller */
    $seller = Seller::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/sellers/sellers:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $seller->{$fieldKey}),
    ]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $seller->id);
})->with([
    ['id', null, null, null],
    ['manager_id', 1, null, null],
    ['legal_name', 'Foo', 'legal_name_like', 'o'],
    ['legal_address', fn () => AddressData::factory()->make(['address_string' => 'Foo']), 'legal_address_like', 'o'],
    ['status', SellerStatusEnum::CREATE, null, [SellerStatusEnum::CREATE, SellerStatusEnum::ACTIVE]],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
], FakerProvider::$optionalDataset);

test('POST /api/v1/sellers/sellers:search 200 sort success', function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;
    Seller::factory()->create();
    postJson('/api/v1/sellers/sellers:search', ['sort' => [$sort]])->assertOk();
})->with([
    'id',
    'manager_id',
    'legal_name',
    'status',
    'created_at',
    'updated_at',
], FakerProvider::$optionalDataset);

<?php

namespace App\Http\ApiV1\Modules\Sellers\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class InnRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (strlen($value) != 10) {
            $fail('Длина ИНН должна быть 10 символов.');

            return;
        }

        $innValue = preg_split('//', $value, 10, PREG_SPLIT_NO_EMPTY);
        [$checkValues, $sourceTotalValue] = array_chunk($innValue, 9);

        $sourceTotalValue = current($sourceTotalValue);
        $totalValue = $this->getTotalValue($checkValues);

        if ($totalValue != $sourceTotalValue) {
            $fail('Проверьте корректность ИНН.');
        }
    }

    protected function getTotalValue(array $checkValues): int
    {
        $coefficients = $this->dischargeCoefficients();

        $totalValue = 0;
        foreach ($checkValues as $key => $checkValue) {
            $totalValue += $checkValue * $coefficients[$key];
        }

        $totalValue = $totalValue % 11;

        return $totalValue == 10 ? 0 : $totalValue;
    }

    protected function dischargeCoefficients(): array
    {
        return [2, 4, 10, 3, 5, 9, 4, 6, 8, 0];
    }
}

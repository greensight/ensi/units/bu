<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Queries;

use App\Domain\SellerUsers\Models\Operator;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OperatorsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Operator::query());

        $this->allowedIncludes(['seller']);

        $this->allowedSorts([
            'id',
            'seller_id',
            'user_id',
            'is_receive_sms',
            'is_main',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('is_receive_sms'),
            AllowedFilter::exact('is_main'),

            ...DateFilter::make('created_at')->gte()->lte(),
            ...DateFilter::make('updated_at')->gte()->lte(),
        ]);

        $this->defaultSort('id');
    }
}

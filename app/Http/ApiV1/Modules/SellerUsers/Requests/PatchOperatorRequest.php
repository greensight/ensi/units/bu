<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Requests;

use App\Domain\SellerUsers\Models\Operator;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchOperatorRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $id = (int)$this->route('id');

        return [
            'seller_id' => ['sometimes', 'integer'],
            'user_id' => ['sometimes', 'integer', Rule::unique(Operator::class)->ignore($id)],
            'is_receive_sms' => ['sometimes', 'boolean'],
            'is_main' => ['sometimes', 'boolean'],
        ];
    }
}

<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Requests;

use App\Domain\SellerUsers\Models\Operator;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOperatorRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'seller_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer', Rule::unique(Operator::class)],
            'is_receive_sms' => ['nullable', 'boolean'],
            'is_main' => ['nullable', 'boolean'],
        ];
    }
}

<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Controllers;

use App\Domain\SellerUsers\Actions\CreateOperatorAction;
use App\Domain\SellerUsers\Actions\DeleteOperatorAction;
use App\Domain\SellerUsers\Actions\PatchOperatorAction;
use App\Http\ApiV1\Modules\SellerUsers\Queries\OperatorsQuery;
use App\Http\ApiV1\Modules\SellerUsers\Requests\CreateOperatorRequest;
use App\Http\ApiV1\Modules\SellerUsers\Requests\PatchOperatorRequest;
use App\Http\ApiV1\Modules\SellerUsers\Resources\OperatorsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class OperatorsController
{
    public function create(CreateOperatorRequest $request, CreateOperatorAction $action): Responsable
    {
        return new OperatorsResource($action->execute($request->validated()));
    }

    public function get(int $id, OperatorsQuery $query): Responsable
    {
        return new OperatorsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchOperatorRequest $request, PatchOperatorAction $action): Responsable
    {
        return new OperatorsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteOperatorAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, OperatorsQuery $query): Responsable
    {
        return OperatorsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
